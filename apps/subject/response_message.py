"""
    Subject Response Message Module

    Description:
    - This module is responsible for subject response messages.

"""

# Importing Python Packages

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class SubjectResponseMessage:
    """
    Subject Response Message Class

    Description:
    - This class is used to define subject response messages.

    """

    SUBJECT_NOT_FOUND: str = "Subject not found"
    SUBJECT_DELETED: str = "Subject deleted successfully"
    SUBJECT_ALREADY_CREATED: str = "Subject already created"


subject_response_message = SubjectResponseMessage()
