"""
    Subject Configuration Module

    Description:
    - This module is responsible for subject configuration.

"""

# Importing Python Packages

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class SubjectConfiguration:
    """
    Subject Settings Class

    Description:
    - This class is used to define subject configurations.

    """

    GRADE: int = 1
    SUBJECT: str = "Mathematics"


subject_configuration = SubjectConfiguration()
