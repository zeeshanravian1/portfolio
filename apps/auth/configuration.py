"""
    Authentication Configuration Module

    Description:
    - This module is responsible for auth configuration.

"""

# Importing Python Packages

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class AuthConfiguration:
    """
    Authentication Settings Class

    Description:
    - This class is used to define auth configurations.

    """

    TOKEN_TYPE: str = "bearer"
    ACCESS_TOKEN: str = "access_token"
    REFRESH_TOKEN: str = "refresh_token"


auth_configuration = AuthConfiguration()
