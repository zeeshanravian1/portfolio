"""
    ChatBot Response Message Module

    Description:
    - This module is responsible for chatbot response messages.

"""

# Importing Python Packages

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class ChatBotResponseMessage:
    """
    ChatBot Response Message Class

    Description:
    - This class is used to define chatbot response messages.

    """

    USER_NOT_FOUND: str = "USER not found"
    WELCOME_MESSAGE: str = "Hello! I'm your friendly learning assistant"
    IRRELAVANT_QUESTION: str = """Sorry, your question is not related to selected subject.
Please ask a user_subject related question."""


chatbot_response_message = ChatBotResponseMessage()
