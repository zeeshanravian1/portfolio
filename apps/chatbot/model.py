"""
    Unit Delivery Plan Models

    Description:
    - This file contains models for chatbot.

"""

# Importing Python Packages
from sqlalchemy import Date, Integer, String, Time, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column

# Importing FastAPI Packages

# Importing Project Files
from database import BaseTable
from apps.user.model import UserTable


# -----------------------------------------------------------------------------


class ChatBotTable(BaseTable):
    """
    ChatBot Table

    Description:
    - This table is used to create chatbot in database.

    """

    user_id: Mapped[int] = mapped_column(
        Integer, ForeignKey(UserTable.id), nullable=False
    )
    date: Mapped[Date] = mapped_column(Date, nullable=False)
    subject: Mapped[str] = mapped_column(String(2_0), nullable=False)
    user_query: Mapped[str] = mapped_column(String(50_000), nullable=True)
    bot_response: Mapped[str] = mapped_column(String(50_000), nullable=True)
    user_query_time: Mapped[Time] = mapped_column(Time, nullable=False)
    bot_response_time: Mapped[Time] = mapped_column(Time, nullable=False)
