"""
    ChatBot Pydantic Schemas

    Description:
    - This module contains all chatbot schemas used by API.

"""

# Importing Python Packages
from pydantic import BaseModel, Field

# Importing FastAPI Packages

# Importing Project Files
from .response_message import chatbot_response_message


# -----------------------------------------------------------------------------


class ChatBotUserQueryReadSchema(BaseModel):
    """
    ChatBot User Query Schema

    Description:
    - This schema is used to validate chatbot user query data returned from
    API.

    """

    response: str = Field(
        min_length=1,
        max_length=50_000,
        example=chatbot_response_message.WELCOME_MESSAGE,
    )

    class Config:
        """
        Pydantic Config

        Description:
        - This class is used to configure Pydantic schema.

        """

        str_strip_whitespace = True
        from_attributes = True
