"""
    ChatBot View Module

    Description:
    - This module generates response of chatbot based on user query.

"""

# Importing Python Packages
from datetime import datetime
from sqlalchemy.ext.asyncio import AsyncSession
from langchain.chat_models import ChatOpenAI
from langchain.schema import HumanMessage


# Importing FastAPI Packages

# Importing Project Files
from core.configuration import core_configuration
from core.schema import CurrentUserReadSchema
from .configuration import chatbot_configuration
from .response_message import chatbot_response_message
from .model import ChatBotTable
from .schema import ChatBotUserQueryReadSchema


# -----------------------------------------------------------------------------


# GenerateUDP class
class ChatBotView:
    """
    Generate Unit Delivery Plan Class

    Description:
    - This class is responsible for generating UDP.

    """

    chat_model = ChatOpenAI(
        temperature="0.9",
        openai_api_key=core_configuration.OPENAI_API_KEY,
    )

    async def chatbot_response(
        self,
        db_session: AsyncSession,
        subject: str,
        user_query: str,
        current_user: CurrentUserReadSchema,
    ) -> ChatBotUserQueryReadSchema:
        """
        ChatBot Response Method

        Description:
        - This method is responsible for generating response of chatbot based
        on user query.

        Parameter:
        - **db_session** (AsyncSession): Database session. **(Required)**
        - **subject** (STR): Subject of user query. **(Required)**
        - **user_query** (STR): User query. **(Required)**
        - **current_user** (CurrentUserReadSchema): Current user.
        **(Required)**

        Return:
        - **response** (STR): Response of chatbot based on user query.

        """

        valid_keywords = chatbot_configuration.SUBJECT_KEYWORDS.get(
            subject, []
        )

        if not any(keyword in user_query for keyword in valid_keywords):
            return {
                "detail": chatbot_response_message.IRRELAVANT_QUESTION.replace(
                    "user_subject", subject
                )
            }

        # Current Date and Time
        current_date_time = datetime.now()
        current_date = current_date_time.date()
        user_time = current_date_time.time()

        response = self.chat_model([HumanMessage(content=user_query)])

        current_date_time = datetime.now()
        chatbot_time = current_date_time.time()

        # Inserting user query in database
        record: ChatBotTable = ChatBotTable(
            user_id=current_user.id,
            date=current_date,
            subject=subject,
            user_query=user_query,
            bot_response=response.content,
            user_query_time=user_time,
            bot_response_time=chatbot_time,
        )

        db_session.add(instance=record)
        await db_session.commit()

        return ChatBotUserQueryReadSchema(response=response.content)


chatbot_view = ChatBotView()
