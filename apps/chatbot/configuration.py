"""
    ChatBot Configuration Module

    Description:
    - This module is responsible for chatbot configuration.

"""

# Importing Python Packages
from typing import Literal

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class ChatBotConfiguration:
    """
    ChatBot Settings Class

    Description:
    - This class is used to define chatbot configurations.

    """

    ALLOWED_SUBJECTS = Literal["math", "english", "science", "history"]
    USER_QUERY: str = "Hello, I need help with math."
    SUBJECT_KEYWORDS = {
        "math": [
            "math",
            "addition",
            "subtraction",
            "multiplication",
            "division",
            "numbers",
            "mathematics",
        ],
        "science": [
            "science",
            "biology",
            "chemistry",
            "physics",
            "experiment",
            "scientific",
        ],
        "english": [
            "english",
            "language",
            "reading",
            "writing",
            "grammar",
            "literature",
        ],
        "history": [
            "history",
            "historical",
            "past",
            "events",
            "timeline",
            "historian",
        ],
    }


chatbot_configuration = ChatBotConfiguration()
