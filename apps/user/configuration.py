"""
    User Configuration Module

    Description:
    - This module is responsible for user configuration.

"""

# Importing Python Packages

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class UserConfiguration:
    """
    User Settings Class

    Description:
    - This class is used to define User configurations.

    """

    USERNAME: str = "user"
    EMAIL: str = "user@email.com"
    PASSWORD: str = "12345@Aa"
    ROLE_ID: int = 1
    USER_COLUMN_NAME: str = "username"


user_configuration = UserConfiguration()
