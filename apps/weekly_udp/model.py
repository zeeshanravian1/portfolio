"""
    Weekly Unit Delivery Plan Models

    Description:
    - This file contains model for weekly plan table.

"""

# Importing Python Packages
from sqlalchemy import ARRAY, Integer, String, Date, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

# Importing FastAPI Packages

# Importing Project Files
from database import BaseTable
from apps.udp.model import GeneralPlanTable
from apps.user.model import UserTable


# -----------------------------------------------------------------------------


class WeeklyPlanTable(BaseTable):
    """
    Weekly Plan Table

    Description:
    - This table is used to create weekly plan in database.

    """

    user_id: Mapped[int] = mapped_column(
        Integer, ForeignKey(UserTable.id), nullable=False
    )
    generalplan_id: Mapped[int] = mapped_column(
        Integer, ForeignKey(GeneralPlanTable.id), nullable=False
    )
    plan_date: Mapped[Date] = mapped_column(Date, nullable=False)
    plan_month: Mapped[str] = mapped_column(String(2_0), nullable=False)
    plan_day: Mapped[str] = mapped_column(String(2_0), nullable=False)
    plan_description: Mapped[str] = mapped_column(
        String(1_000), nullable=False
    )
    sub_plans_description: Mapped[ARRAY[str]] = mapped_column(
        ARRAY(String(1_000)), nullable=True
    )
    notes: Mapped[str] = mapped_column(String(10_000), nullable=True)
    status: Mapped[str] = mapped_column(
        String(2_0), nullable=False, server_default="pending"
    )

    # Relationship
    weekly_plan_generalplan = relationship(
        "GeneralPlanTable",
        back_populates="generalplan_weeklyplan",
        lazy="joined",
    )
    weeklyplan_dailyplan = relationship(
        "DailyPlanTable", back_populates="daily_plan_weeklyplan"
    )
