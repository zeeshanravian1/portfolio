"""
    Weekly Unit Delivery Plan Configuration Module

    Description:
    - This module is responsible for weekly udp configuration.

"""

# Importing Python Packages
from datetime import date

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class WeeklyUDPConfiguration:
    """
    Weekly Unit Delivery Plan Settings Class

    Description:
    - This class is used to define weekly udp configurations.

    """

    ID: int = 1
    DAY: str = "Sunday"
    MONTH: str = "January"
    DATE: date = date.today()
    NOTES: str = "Additional notes"
    PLAN_DESCRIPTION: str = "Plan description"
    SUB_PLAN_DESCRIPTION: list = [
        "Sub plan description",
        "Sub plan description",
    ]
    STATUS: str = "Pending"

    ID_COLUMN_NAME: str = "id"
    GENERAL_PLAN_ID_COLUMN_NAME: str = "generalplan_id"


weekly_udp_configuration = WeeklyUDPConfiguration()
