"""
    Weekly Unit Delivery Plan Route Module

    Description:
    - This module is responsible for handling weekly udp routes.
    - It is used to create, get, update, delete weekly udp details.

"""

# Importing Python Packages
from sqlalchemy.ext.asyncio import AsyncSession

# Importing FastAPI Packages
from fastapi import APIRouter, Depends, Security, status
from fastapi.responses import JSONResponse

# Importing Project Files
from database.session import get_session
from core.security import get_current_user
from core.schema import CurrentUserReadSchema
from .configuration import weekly_udp_configuration
from .response_message import weekly_udp_response_message
from .model import WeeklyPlanTable
from .schema import WeeklyUDPReadSchema, WeeklyUDPPatchSchema
from .view import weekly_udp_view

# Router Object to Create Routes
router = APIRouter(prefix="/weekly-udp", tags=["Weekly UDP"])


# -----------------------------------------------------------------------------


# Create weekly udp
@router.post(
    path="/{generalplan_id}/",
    status_code=status.HTTP_201_CREATED,
    summary="Create a weekly udp",
    response_description="Weekly udp created successfully",
)
async def create_weekly_udp(
    generalplan_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> WeeklyUDPReadSchema:
    """
    Create a weekly udp

    Description:
    - This route is used to create a weekly udp.

    Parameter:
    - **generalplan_id** (INT): Id of general plan. **(Required)**

    Return:
    Weekly udp details along with following information:
    - **generalplan_id** (INT): Id of general plan.
    - **notes** (STR): Additional notes.
    - **weekly_udp** (LIST): List of weekly udp.
        - Each weekly udp contains following information:
            - **id** (INT): Id of weekly udp.
            - **plan_date** (DATE): Date of weekly udp.
            - **plan_month** (STR): Month of weekly udp.
            - **plan_day** (STR): Day of weekly udp.
            - **plan_description** (STR): Description of weekly udp.
            - **sub_plans_description** (LIST): List of sub plans description.
            - **status** (STR): Status of weekly udp.
            - **created_at** (DATETIME): Datetime of weekly udp creation.
            - **updated_at** (DATETIME): Datetime of weekly udp updation.

    """
    print("Calling create_weekly_udp method")

    result: WeeklyUDPReadSchema = await weekly_udp_view.create(
        generalplan_id=generalplan_id,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, WeeklyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "detail": weekly_udp_response_message.GENERAL_PLAN_NOT_FOUND
            },
        )

    return WeeklyUDPReadSchema.model_validate(obj=result)


# Get weekly udp by id route
@router.get(
    path="/{weekly_udp_id}/",
    status_code=status.HTTP_200_OK,
    summary="Get a single weekly udp by providing id",
    response_description="Weekly udp details fetched successfully",
)
async def get_weekly_udp_by_id(
    weekly_udp_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> WeeklyUDPReadSchema:
    """
    Get a single weekly udp.

    Description:
    - This route is used to get a single weekly udp by providing id.

    Parameter:
    - **weekly_udp_id** (INT): ID of weekly udp to be fetched. **(Required)**

    Return:
    Get a single weekly udp with following information:
    - **generalplan_id** (INT): Id of general plan.
    - **notes** (STR): Additional notes.
    - **weekly_udp** (LIST): List of weekly udp.
        - Each weekly udp contains following information:
            - **id** (INT): Id of weekly udp.
            - **plan_date** (DATE): Date of weekly udp.
            - **plan_month** (STR): Month of weekly udp.
            - **plan_day** (STR): Day of weekly udp.
            - **plan_description** (STR): Description of weekly udp.
            - **sub_plans_description** (LIST): List of sub plans description.
            - **status** (STR): Status of weekly udp.
            - **created_at** (DATETIME): Datetime of weekly udp creation.
            - **updated_at** (DATETIME): Datetime of weekly udp updation.

    """
    print("Calling get_weekly_udp_by_id method")

    result: WeeklyUDPReadSchema = await weekly_udp_view.read_by_id(
        record_id=weekly_udp_id,
        column_name=weekly_udp_configuration.ID_COLUMN_NAME,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, WeeklyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "detail": weekly_udp_response_message.WEEKLY_UDP_NOT_FOUND
            },
        )

    return WeeklyUDPReadSchema.model_validate(obj=result)


# Get all weekly udp by generalplan id route
@router.get(
    path="/all/{generalplan_id}/",
    status_code=status.HTTP_200_OK,
    summary="Get all weekly udp by providing generalplan id",
    response_description="All weekly udps fetched successfully",
)
async def get_all_weekly_udp_by_generalplan_id(
    generalplan_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> WeeklyUDPReadSchema:
    """
    Get all weekly udp by generalplan id.

    Description:
    - This route is used to get a single weekly udp by providing id.

    Parameter:
    - **generalplan_id** (INT): ID of general udp to be fetched. **(Required)**

    Return:
    Get all weekly udp with following information:
    - **generalplan_id** (INT): Id of general plan.
    - **notes** (STR): Additional notes.
    - **weekly_udp** (LIST): List of weekly udp.
        - Each weekly udp contains following information:
            - **id** (INT): Id of weekly udp.
            - **plan_date** (DATE): Date of weekly udp.
            - **plan_month** (STR): Month of weekly udp.
            - **plan_day** (STR): Day of weekly udp.
            - **plan_description** (STR): Description of weekly udp.
            - **sub_plans_description** (LIST): List of sub plans description.
            - **status** (STR): Status of weekly udp.
            - **created_at** (DATETIME): Datetime of weekly udp creation.
            - **updated_at** (DATETIME): Datetime of weekly udp updation.

    """
    print("Calling get_all_weekly_udp_by_generalplan_id method")

    result: WeeklyUDPReadSchema = await weekly_udp_view.read_by_id(
        record_id=generalplan_id,
        column_name=weekly_udp_configuration.GENERAL_PLAN_ID_COLUMN_NAME,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, WeeklyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "detail": weekly_udp_response_message.GENERAL_PLAN_NOT_FOUND
            },
        )

    return WeeklyUDPReadSchema.model_validate(obj=result)


# Partial update a single weekly udp route
@router.patch(
    path="/{weekly_udp_id}/",
    status_code=status.HTTP_202_ACCEPTED,
    summary="Partially update a single weekly udp by providing id",
    response_description="Weekly udp partial updated successfully",
)
async def partial_update_weekly_udp(
    weekly_udp_id: int,
    record: WeeklyUDPPatchSchema,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> WeeklyUDPReadSchema:
    """
    Partial update a single weekly udp.

    Description:
    - This route is used to partial update a single weekly udp by providing id.

    Parameter:
    - **weekly_udp_id** (INT): Id of weekly udp. **(Required)**
    - **notes** (STR): Additional notes. **(Optional)**
    - **plan_date** (DATE): Date of weekly udp. **(Optional)**
    - **plan_month** (STR): Month of weekly udp. **(Optional)**
    - **plan_day** (STR): Day of weekly udp. **(Optional)**
    - **plan_description** (STR): Description of weekly udp.
    **(Optional)**
    - **sub_plans_description** (LIST): List of sub plans description.
    **(Optional)**
    - **status** (STR): Status of weekly udp. **(Optional)**

    Return:
    Weekly udp details along with following information:
    - **generalplan_id** (INT): Id of general plan.
    - **notes** (STR): Additional notes.
    - **weekly_udp** (LIST): List of weekly udp.
        - Each weekly udp contains following information:
            - **id** (INT): Id of weekly udp.
            - **plan_date** (DATE): Date of weekly udp.
            - **plan_month** (STR): Month of weekly udp.
            - **plan_day** (STR): Day of weekly udp.
            - **plan_description** (STR): Description of weekly udp.
            - **sub_plans_description** (LIST): List of sub plans description.
            - **status** (STR): Status of weekly udp.
            - **created_at** (DATETIME): Datetime of weekly udp creation.
            - **updated_at** (DATETIME): Datetime of weekly udp updation.

    """
    print("Calling partial_update_weekly_udp method")

    result: WeeklyUDPReadSchema = await weekly_udp_view.update(
        record_id=weekly_udp_id,
        record=record,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, WeeklyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "detail": weekly_udp_response_message.GENERAL_PLAN_NOT_FOUND
            },
        )

    return WeeklyUDPReadSchema.model_validate(obj=result)


# Delete a single weekly udp by id route
@router.delete(
    path="/{weekly_udp_id}/",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete a single weekly udp by providing id",
    response_description="Weekly udp deleted successfully",
)
async def delete_weekly_udp(
    weekly_udp_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> None:
    """
    Delete a single weekly udp.

    Description:
    - This route is used to delete a single weekly udp by providing id.

    Parameter:
    - **weekly_udp_id** (INT): ID of weekly udp to be deleted. **(Required)**

    Return:
    - **None**

    """
    print("Calling delete_weekly_udp method")

    result: WeeklyPlanTable = await weekly_udp_view.delete(
        record_id=weekly_udp_id,
        column_name=weekly_udp_configuration.ID_COLUMN_NAME,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, WeeklyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "detail": weekly_udp_response_message.WEEKLY_UDP_NOT_FOUND
            },
        )


# Delete all weekly udps by generalplan id route
@router.delete(
    path="/all/{generalplan_id}/",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete all weekly udps by providing id",
    response_description="Weekly udps deleted successfully",
)
async def delete_all_weekly_udp(
    generalplan_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> None:
    """
    Delete all weekly udps.

    Description:
    - This route is used to delete all weekly udps by providing general plan
    id.

    Parameter:
    - **generalplan_id** (INT): ID of general plan to be deleted.
    **(Required)**

    Return:
    - **None**

    """
    print("Calling delete_all_weekly_udp method")

    result: WeeklyPlanTable = await weekly_udp_view.delete(
        record_id=generalplan_id,
        column_name=weekly_udp_configuration.GENERAL_PLAN_ID_COLUMN_NAME,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, WeeklyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "detail": weekly_udp_response_message.GENERAL_PLAN_NOT_FOUND
            },
        )
