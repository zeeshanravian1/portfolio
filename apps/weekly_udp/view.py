"""
    Weekly Unit Delivery Plan View Module

    Description:
    - This module is responsible for weekly udp views.

"""

# Importing Python Packages
from datetime import timedelta
import re
from sqlalchemy import select, update, delete
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.engine.result import ChunkedIteratorResult
from sqlalchemy.sql.selectable import Select
from sqlalchemy.sql.dml import Update, Delete
from langchain import LLMChain
from langchain.chat_models import ChatOpenAI
from langchain.prompts import PromptTemplate
from langchain.callbacks import get_openai_callback

# Importing FastAPI Packages

# Importing Project Files
from core.configuration import core_configuration
from core.schema import CurrentUserReadSchema
from apps.udp.model import GeneralPlanTable
from .configuration import weekly_udp_configuration
from .response_message import weekly_udp_response_message
from .model import WeeklyPlanTable
from .schema import (
    WeeklyUDPDataSchema,
    WeeklyUDPReadSchema,
    WeeklyUDPPatchSchema,
)


# -----------------------------------------------------------------------------


# WeeklyUDP View class
class WeeklyUDPView:
    """
    Weekly Unit Delivery Plan View Class

    Description:
    - This class is responsible for weekly udp views.

    """

    llm = ChatOpenAI(
        temperature=0,
        openai_api_key=core_configuration.OPENAI_API_KEY,
        model_name=core_configuration.MODEL_NAME,
    )

    async def create(
        self,
        generalplan_id: int,
        db_session: AsyncSession,
        current_user: CurrentUserReadSchema,
    ) -> WeeklyUDPReadSchema:
        """
        Create a weekly udp

        Description:
        - This method is responsible for creating weekly udp.

        Parameter:
        - **generalplan_id** (INT): Id of general plan. **(Required)**
        - **db_session** (Session): Database session. **(Required)**
        - **current_user** (CurrentUserReadSchema): Current user details.
        **(Required)**

        Return:
        Weekly udp details along with following information:
        - **generalplan_id** (INT): Id of general plan.
        - **notes** (STR): Additional notes.
        - **weekly_udp** (LIST): List of weekly udp.
            - Each weekly udp contains following information:
                - **id** (INT): Id of weekly udp.
                - **plan_date** (DATE): Date of weekly udp.
                - **plan_month** (STR): Month of weekly udp.
                - **plan_day** (STR): Day of weekly udp.
                - **plan_description** (STR): Description of weekly udp.
                - **sub_plans_description** (LIST): List of sub plans
                description.
                - **status** (STR): Status of weekly udp.
                - **created_at** (DATETIME): Datetime of weekly udp creation.
                - **updated_at** (DATETIME): Datetime of weekly udp updation.

        """

        # Check if general plan exists
        query: Select = select(GeneralPlanTable).where(
            GeneralPlanTable.id == generalplan_id
        )
        result: ChunkedIteratorResult = await db_session.execute(
            statement=query
        )
        generalplan_result: GeneralPlanTable = result.scalars().first()

        if not generalplan_result:
            return {
                "detail": weekly_udp_response_message.GENERAL_PLAN_NOT_FOUND
            }

        response = {
            "generalplan_id": generalplan_id,
            "notes": None,
            "weekly_udp": [],
        }

        # Specifiying how weekly plan should be structured
        template = """You are an Educational Planner Chatbot creating a Unit Development Plan for students in {grade_level} for the subject of Mathematics.

Using the overarching Unit Delivery Plan (UDP) overview, construct a weekly plan starting from Sunday to Thursday using Common Core Standards in the following structured format:

Heading:
Weekly Plan for Common Core Standard:{topic} - Dated: {month},{year}.

Title:
State what we are going to study aligned with common core standards.

Day-by-Day topic breakdown:
State only the daily Topic plan for topic {topic} for the whole week. Start with Sunday, then Monday, and so on.

Educational Planner Chatbot:
                   """

        # Initializing prompt
        prompt = PromptTemplate(
            template=template,
            input_variables=[
                "grade_level",
                "month",
                "year",
                "topic",
            ],
        )

        # Initilaize LLM chain
        llm_chain = LLMChain(llm=self.llm, prompt=prompt)

        with get_openai_callback():
            result = llm_chain(
                {
                    "grade_level": generalplan_result.generalplan_subject.grade_level,
                    "month": generalplan_result.current_start_month,
                    "year": generalplan_result.start_date.year,
                    "topic": generalplan_result.plan_description,
                },
                return_only_outputs=True,
            )

            weekly_plan = result["text"]

            # Extract additional notes
            pattern = re.compile(
                r"(Note|Notes|Additional Note|Additional Notes):\n*(.+)"
            )
            match = pattern.search(weekly_plan)

            if match:
                response["notes"] = match.group(2).strip()

            # Extract plans
            pattern = re.compile(
                r"(\w+):\s*\n(Topic:\s*([^\n]+))([\s\S]*?)(?=(?:\w+:|$))"
            )
            matches = pattern.findall(weekly_plan)

            start_date = generalplan_result.start_date
            index = 0  # Number of plan dates added so far

            while index < 5:
                if start_date.strftime("%A") not in ["Friday", "Saturday"]:
                    response["weekly_udp"].append(
                        {
                            "plan_date": start_date,
                            "plan_month": start_date.strftime("%B"),
                            "plan_day": start_date.strftime("%A"),
                            "plan_description": matches[index][2].strip(),
                            "sub_plans_description": [
                                subtopic.strip()
                                for subtopic in matches[index][3].split("\n-")
                                if subtopic.strip()
                            ],
                        }
                    )

                    index += 1
                start_date += timedelta(days=1)

        # Insert into database
        inserted_record = [
            WeeklyPlanTable(
                user_id=current_user.id,
                generalplan_id=generalplan_id,
                plan_date=data["plan_date"],
                plan_month=data["plan_month"],
                plan_day=data["plan_day"],
                plan_description=data["plan_description"],
                sub_plans_description=data["sub_plans_description"],
                notes=response["notes"],
            )
            for data in response.get("weekly_udp")
        ]

        db_session.add_all(instances=inserted_record)
        await db_session.commit()

        for record in inserted_record:
            await db_session.refresh(instance=record)

        return WeeklyUDPReadSchema(
            generalplan_id=generalplan_id,
            notes=response["notes"],
            weekly_udp=[
                WeeklyUDPDataSchema(
                    id=record.id,
                    created_at=record.created_at,
                    updated_at=record.updated_at,
                    plan_date=record.plan_date,
                    plan_month=record.plan_month,
                    plan_day=record.plan_day,
                    plan_description=record.plan_description,
                    sub_plans_description=record.sub_plans_description,
                    status=record.status,
                )
                for record in inserted_record
            ],
        )

    async def read_by_id(
        self,
        record_id: int,
        column_name: str,
        db_session: AsyncSession,
        current_user: CurrentUserReadSchema,
    ) -> WeeklyUDPReadSchema:
        """
        Get a single weekly udp.

        Description:
        - This method is responsible for reading a record by ID.

        Parameter:
        - **record_id** (INT): ID of weekly udp to be fetched. **(Required)**
        - **column_name** (STR): Column name. **(Required)**
        - **db_session** (Session): Database session. **(Required)**
        - **current_user** (CurrentUserReadSchema): Current user details.
        **(Required)**

        Return:
        Weekly udp details along with following information:
        - **generalplan_id** (INT): Id of general plan.
        - **notes** (STR): Additional notes.
        - **weekly_udp** (LIST): List of weekly udp.
            - Each weekly udp contains following information:
                - **id** (INT): Id of weekly udp.
                - **plan_date** (DATE): Date of weekly udp.
                - **plan_month** (STR): Month of weekly udp.
                - **plan_day** (STR): Day of weekly udp.
                - **plan_description** (STR): Description of weekly udp.
                - **sub_plans_description** (LIST): List of sub plans
                description.
                - **status** (STR): Status of weekly udp.
                - **created_at** (DATETIME): Datetime of weekly udp creation.
                - **updated_at** (DATETIME): Datetime of weekly udp updation.

        """

        query: Select = select(WeeklyPlanTable).where(
            getattr(WeeklyPlanTable, column_name) == record_id,
            WeeklyPlanTable.user_id == current_user.id,
        )
        result: ChunkedIteratorResult = await db_session.execute(
            statement=query
        )
        weeklyplan_result: list = result.scalars().all()

        if not weeklyplan_result:
            return {"detail": weekly_udp_response_message.WEEKLY_UDP_NOT_FOUND}

        return WeeklyUDPReadSchema(
            generalplan_id=weeklyplan_result[0].generalplan_id,
            notes=weeklyplan_result[0].notes,
            weekly_udp=[
                WeeklyUDPDataSchema(
                    id=record.id,
                    plan_date=record.plan_date,
                    plan_month=record.plan_month,
                    plan_day=record.plan_day,
                    plan_description=record.plan_description,
                    sub_plans_description=record.sub_plans_description,
                    status=record.status,
                    created_at=record.created_at,
                    updated_at=record.updated_at,
                )
                for record in weeklyplan_result
            ],
        )

    async def update(
        self,
        record_id: int,
        record: WeeklyUDPPatchSchema,
        db_session: AsyncSession,
        current_user: CurrentUserReadSchema,
    ) -> WeeklyUDPReadSchema:
        """
        Update a single weekly udp.

        Description:
        - This method is responsible for updaing a record by ID.

        Parameter:
        - **record_id** (INT): ID of weekly udp to be update. **(Required)**
        - **db_session** (Session): Database session. **(Required)**
        - **current_user** (CurrentUserReadSchema): Current user details.
        **(Required)**
        - **notes** (STR): Additional notes. **(Optional)**
        - **plan_date** (DATE): Date of weekly udp. **(Optional)**
        - **plan_month** (STR): Month of weekly udp. **(Optional)**
        - **plan_day** (STR): Day of weekly udp. **(Optional)**
        - **plan_description** (STR): Description of weekly udp.
        **(Optional)**
        - **sub_plans_description** (LIST): List of sub plans description.
        **(Optional)**
        - **status** (STR): Status of weekly udp. **(Optional)**

        Return:
        Weekly udp details along with following information:
        - **generalplan_id** (INT): Id of general plan.
        - **notes** (STR): Additional notes.
        - **weekly_udp** (LIST): List of weekly udp.
            - Each weekly udp contains following information:
                - **id** (INT): Id of weekly udp.
                - **plan_date** (DATE): Date of weekly udp.
                - **plan_month** (STR): Month of weekly udp.
                - **plan_day** (STR): Day of weekly udp.
                - **plan_description** (STR): Description of weekly udp.
                - **sub_plans_description** (LIST): List of sub plans
                description.
                - **status** (STR): Status of weekly udp.
                - **created_at** (DATETIME): Datetime of weekly udp creation.
                - **updated_at** (DATETIME): Datetime of weekly udp updation.

        """

        query: Update = (
            update(WeeklyPlanTable)
            .where(WeeklyPlanTable.id == record_id)
            .values(record.model_dump(exclude_unset=True))
        )
        await db_session.execute(statement=query)
        await db_session.commit()

        return await self.read_by_id(
            record_id=record_id,
            column_name=weekly_udp_configuration.ID_COLUMN_NAME,
            db_session=db_session,
            current_user=current_user,
        )

    async def delete(
        self,
        record_id: int,
        column_name: str,
        db_session: AsyncSession,
        current_user: CurrentUserReadSchema,
    ) -> WeeklyPlanTable:
        """
        Delete Method

        Description:
        - This method is responsible for deleting a record.

        Parameter:
        - **record_id** (int): Record ID. **(Required)**
        - **column_name** (STR): Column name. **(Required)**
        - **db_session** (Session): Database session. **(Required)**
        - **current_user** (CurrentUserReadSchema): Current user details.
        **(Required)**

        Return:
        - **record** (WeeklyPlanTable): SqlAlchemy Model Object.

        """

        result: WeeklyPlanTable = await self.read_by_id(
            record_id=record_id,
            column_name=column_name,
            db_session=db_session,
            current_user=current_user,
        )

        if not isinstance(result, WeeklyUDPReadSchema):
            return result

        query: Delete = delete(WeeklyPlanTable).where(
            getattr(WeeklyPlanTable, column_name) == record_id,
        )
        await db_session.execute(statement=query)
        await db_session.commit()

        return result


weekly_udp_view = WeeklyUDPView()
