"""
    Weekly Unit Delivery Plan Pydantic Schemas

    Description:
    - This module contains all weekly udp schemas used by API.

"""

# Importing Python Packages
from datetime import date
from pydantic import BaseModel, Field
from pydantic_settings import SettingsConfigDict

# Importing FastAPI Packages

# Importing Project Files
from apps.base import BaseReadSchema
from .configuration import weekly_udp_configuration


# -----------------------------------------------------------------------------


class WeeklyUDPDataSchema(BaseReadSchema, BaseModel):
    """
    Weekly Unit Delivery Plan Data Schema

    Description:
    - This schema is used to validate weekly udp data returned from API.

    """

    plan_date: date | None = Field(
        default=None,
        example=weekly_udp_configuration.DATE,
    )
    plan_month: str | None = Field(
        default=None,
        min_length=1,
        max_length=2_0,
        example=weekly_udp_configuration.MONTH,
    )
    plan_day: str | None = Field(
        default=None,
        min_length=1,
        max_length=2_0,
        example=weekly_udp_configuration.DAY,
    )
    plan_description: str | None = Field(
        default=None,
        min_length=1,
        max_length=1_000,
        example=weekly_udp_configuration.PLAN_DESCRIPTION,
    )
    sub_plans_description: list[str] | None = Field(
        default=None,
        min_length=1,
        max_length=1_000,
        example=weekly_udp_configuration.SUB_PLAN_DESCRIPTION,
    )
    status: str | None = Field(
        default=None,
        min_length=1,
        max_length=2_0,
        example=weekly_udp_configuration.STATUS,
    )

    # CoreConfiguration Configuration
    model_config = SettingsConfigDict(
        str_strip_whitespace=True, from_attributes=True
    )


class WeeklyUDPReadSchema(BaseModel):
    """
    Weekly Unit Delivery Plan Read Schema

    Description:
    - This schema is used to validate weekly udp returned from API.

    """

    generalplan_id: int = Field(ge=1, example=weekly_udp_configuration.ID)
    notes: str | None = Field(
        default=None,
        min_length=1,
        max_length=10_000,
        example=weekly_udp_configuration.NOTES,
    )
    weekly_udp: list[WeeklyUDPDataSchema]

    # CoreConfiguration Configuration
    model_config = SettingsConfigDict(
        str_strip_whitespace=True, from_attributes=True
    )


class WeeklyUDPPatchSchema(BaseModel):
    """
    Weekly Unit Delivery Plan patch Schema

    Description:
    - This schema is used to validate weekly udp passed to API.

    """

    notes: str | None = Field(
        default=None,
        min_length=1,
        max_length=10_000,
        example=weekly_udp_configuration.NOTES,
    )
    plan_date: date | None = Field(
        default=None,
        example=weekly_udp_configuration.DATE,
    )
    plan_month: str | None = Field(
        default=None,
        min_length=1,
        max_length=2_0,
        example=weekly_udp_configuration.MONTH,
    )
    plan_day: str | None = Field(
        default=None,
        min_length=1,
        max_length=2_0,
        example=weekly_udp_configuration.DAY,
    )
    plan_description: str | None = Field(
        default=None,
        min_length=1,
        max_length=1_000,
        example=weekly_udp_configuration.PLAN_DESCRIPTION,
    )
    sub_plans_description: list[str] | None = Field(
        default=None,
        min_length=1,
        max_length=1_000,
        example=weekly_udp_configuration.SUB_PLAN_DESCRIPTION,
    )
    status: str | None = Field(
        default=None,
        min_length=1,
        max_length=2_0,
        example=weekly_udp_configuration.STATUS,
    )

    # CoreConfiguration Configuration
    model_config = SettingsConfigDict(
        str_strip_whitespace=True, from_attributes=True
    )
