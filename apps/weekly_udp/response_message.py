"""
    Weekly Unit Delivery Plan Response Message Module

    Description:
    - This module is responsible for weekly udp response messages.

"""

# Importing Python Packages

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class WeeklyUDPResponseMessage:
    """
    Weekly Unit Delivery Plan Response Message Class

    Description:
    - This class is used to define weekly udp response messages.

    """

    GENERAL_PLAN_NOT_FOUND: str = "General plan not found"
    WEEKLY_UDP_NOT_FOUND: str = "Weekly udp not found"


weekly_udp_response_message = WeeklyUDPResponseMessage()
