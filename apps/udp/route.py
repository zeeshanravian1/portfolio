"""
    Unit Delivery Plan Route Module

    Description:
    - This module is responsible for handling udp routes.

"""

# Importing Python Packages
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.sql import func


# Importing FastAPI Packages
from fastapi import APIRouter, Depends, Security, Query, status, HTTPException
from fastapi.responses import JSONResponse

# Importing Project Files
from database.session import get_session
from core.security import get_current_user
from core.schema import CurrentUserReadSchema
from .configuration import udp_configuration
from .response_message import udp_response_message
from .schema import (
    GeneralUDPReadSchema,
    GeneralUDPDataSchema,
    GeneralPlanSummaryListSchema,
    GeneralPlanSummarySchema,
    GeneralUDPDataUpdateSchema,
)
from .model import GeneralPlanTable
from apps.subject.model import SubjectTable
from sqlalchemy import select, delete
from .view import udp_view
from typing import List

# Router Object to Create Routes
router = APIRouter(prefix="/general-plans-udp", tags=["General Plans UDP"])


# -----------------------------------------------------------------------------
@router.get(
    path="/",
    response_model=GeneralPlanSummaryListSchema,
    status_code=status.HTTP_200_OK,
    summary="Get all general plans",
    response_description="List of all general plans",
)
async def get_all_general_plans(
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> GeneralPlanSummaryListSchema:
    """
    Get all plans.

    Description:
    - This route is used to get all plans.

    Return:
    Get all plans with the following information:
    - **id** (INT): Id of the plan.
    - **grade** (INT): Grade.
    - **subject** (STR): Subject selected.
    - **start_date** (STR): Start date of the plan.
    - **status** (STR): Status of the plan.

    """
    # Query the database to fetch distinct general plans based on subject_id and grade_level
    query = select(
        SubjectTable.id,
        SubjectTable.grade_level,
        func.min(GeneralPlanTable.start_date).label(
            "start_date"
        ),  # Use MIN aggregate function
        SubjectTable.subject_name,
    ).join(GeneralPlanTable)

    # Group by subject_id and grade_level
    query = query.group_by(SubjectTable.id, SubjectTable.grade_level)

    result = await db_session.execute(query)
    general_plans = result.all()

    # Convert the database results to GeneralPlanSummarySchema objects
    general_plans_data = [
        GeneralPlanSummarySchema(
            id=plan.id,  # You can use subject_id as id
            grade=plan.grade_level,
            start_date=plan.start_date.strftime("%Y-%m-%d"),
            subject=plan.subject_name,
            status="Pending",
        )
        for plan in general_plans
    ]

    return GeneralPlanSummaryListSchema(general_plans=general_plans_data)


@router.get(
    "/subject/{subject_id}",
    response_model=List[GeneralPlanSummarySchema],
    status_code=status.HTTP_200_OK,
    summary="Get all plans for a specific subject",
    response_description="List of plans for a specific subject",
)
async def get_plans_by_subject(
    subject_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> List[GeneralPlanSummarySchema]:
    """
    Get all plans for a specific subject.

    Description:
    - This route is used to get all plans for a specific subject.

    Parameters:
    - **subject_id** (INT): ID of the subject for which plans need to be retrieved. **(Required)**

    Return:
    Get all plans for the specified subject with the following information:
    - **id** (INT): ID of the plan.
    - **grade** (INT): Grade.
    - **subject** (STR): Subject selected.
    - **start_date** (STR): Start date of the plan.
    - **status** (STR): Status of the plan.

    """
    # Query the database to fetch all plans associated with the specified subject_id
    query = (
        select(
            GeneralPlanTable.id,
            SubjectTable.grade_level.label(
                "grade"
            ),  # Rename grade_level to grade
            SubjectTable.subject_name.label(
                "subject"
            ),  # Rename subject_name to subject
            GeneralPlanTable.start_date,
            GeneralPlanTable.status,
        )
        .join(GeneralPlanTable, SubjectTable.id == GeneralPlanTable.subject_id)
        .filter(GeneralPlanTable.subject_id == subject_id)
    )

    result = await db_session.execute(query)
    general_plans = result.all()

    # Convert the database results to GeneralPlanSummarySchema objects
    general_plans_data = [
        GeneralPlanSummarySchema(
            id=plan.id,
            grade=plan.grade,
            start_date=plan.start_date.strftime("%Y-%m-%d"),
            subject=plan.subject,  # Access the subject_name from SubjectTable
            status=plan.status,
        )
        for plan in general_plans
    ]

    return general_plans_data


@router.delete(
    "/{subject_id}",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete all plans related to a subject by ID",
    response_description="Plans deleted successfully",
)
async def delete_plans(
    subject_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
):
    """
    Delete a single plan.

    Description:
    - This route is used to delete a all plans of that specific subject by providing id.

    Parameter:
    - **subject_id** (INT): ID of subject whose plans to be deleted.
    **(Required)**

    Return:
    - **None**

    """
    # Check if the subject with the provided ID exists
    subject_query = select(SubjectTable).where(SubjectTable.id == subject_id)
    subject_result = await db_session.execute(subject_query)
    existing_subject = subject_result.scalar()

    if existing_subject is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Subject with ID {subject_id} not found",
        )

    # Delete all plans related to the subject_id
    plans_query = delete(GeneralPlanTable).where(
        GeneralPlanTable.subject_id == subject_id
    )
    await db_session.execute(plans_query)

    await db_session.commit()

    return None


@router.get(
    "/{plan_id}/details",
    response_model=GeneralUDPDataSchema,
    status_code=status.HTTP_200_OK,
    summary="Get detailed information of a specific general plan",
    response_description="Detailed information of a specific general plan",
)
async def get_detailed_general_plan(
    plan_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> GeneralUDPDataSchema:
    """
    Generate General UDP

    Description:
    - This method is responsible for getting detail information of certain plan.

    Parameter:
    - **plan_id** (INT): Id of Plan. **(Required)**

    Return:
    General udp details along with following information:
    - **id** (INT): Id of plan.
    - **week_number** (STR): week number.
    - **end_date** (STR): Ending date of plan.
    - **start_date** (STR): start date.
    - **current_start_month** (STR): current start month.
    - **current_end_date** (STR): current end date.
    - **status** (STR): status of the plan.

    """
    # Query the database to fetch the detailed information of the specific general plan
    query = select(GeneralPlanTable).where(GeneralPlanTable.id == plan_id)
    result = await db_session.execute(query)
    general_plan = result.scalars().first()

    if not general_plan:
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={"detail": "General plan not found"},
        )

    # Convert the database result to a GeneralUDPDataSchema object
    general_plan_data = GeneralUDPDataSchema(
        id=general_plan.id,
        week_number=general_plan.week_number,
        plan_description=general_plan.plan_description,
        start_date=general_plan.start_date,
        end_date=general_plan.end_date,
        current_start_month=general_plan.current_start_month,
        current_end_month=general_plan.current_end_month,
        status=general_plan.status,
    )

    return general_plan_data


@router.put(
    "/{plan_id}/details",
    status_code=status.HTTP_200_OK,
    summary="Update detailed information of a specific general plan",
    response_description="General plan details updated successfully",
    response_model=GeneralUDPDataSchema,
)
async def update_general_plan_details(
    plan_id: int,
    plan_update: GeneralUDPDataUpdateSchema,  # Replace with your update schema
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
):
    """
    Update General UDP

    Description:
    - This method is responsible for updating detail information of certain plan.

    Parameter:
    - **plan_id** (INT): Id of Plan. **(Required)**

    Return:
    General udp details along with following information:
    - **id** (INT): Id of plan.
    - **week_number** (DATE): week number.
    - **end_date** (DATE): Ending date of plan.
    - **start_date** (STR): start date.
    - **current_start_month** (STR): current start month.
    - **current_end_date** (STR): current end date.
    - **status** (STR): status of the plan.


    """
    # Query the general plan by ID asynchronously
    existing_plan = await db_session.execute(
        select(GeneralPlanTable).where(
            GeneralPlanTable.id == plan_id
        )  # Lock the row for update
    )

    existing_plan = existing_plan.scalar_one_or_none()

    if existing_plan is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"General plan with ID {plan_id} not found",
        )

    # Update the plan details with the new data
    for field, value in plan_update.dict().items():
        setattr(existing_plan, field, value)

    # The transaction is already managed by the async context manager
    await db_session.commit()

    return existing_plan


# -----------------------------------------------------------------------------


# Generate general udp
@router.get(
    path="/general/",
    status_code=status.HTTP_200_OK,
    summary="Generate general udp",
    response_description="General udp generated successfully",
)
async def generate_general_udp(
    subject_id: int = Query(example=udp_configuration.SUBJECT_ID),
    start_month: str = Query(example=udp_configuration.START_MONTH),
    end_month: str = Query(example=udp_configuration.END_MONTH),
    sub_topic: str = Query(example=udp_configuration.SUB_TOPIC),
    status: str = Query(example=udp_configuration.STATUS),
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> GeneralUDPReadSchema:
    """
    Generate General UDP

    Description:
    - This method is responsible for generating general udp.

    Parameter:
    - **subject_id** (INT): Id of Subject. **(Required)**
    - **start_month** (STR): Starting month of unit. **(Required)**
    - **end_month** (STR): Ending month of unit. **(Required)**
    - **sub_topic** (STR): sub topic. **(Required)**
    - **status** (STR): status. **(Required)**

    Return:
    General udp details along with following information:
    - **subject_id** (INT): Id of Subject.
    - **start_month** (STR): Starting month of unit.
    - **end_month** (STR): Ending month of unit.
    - **unit_duration** (STR): Duration of unit.
    - **class_size** (INT): Number of students in class.
    - **assessment_details** (STR): Generated assessment details.
    - **notes** (STR): Generated additional notes.
    - **general_udp** (LIST): Generated general udp.
        - Each general udp contains following information:
            - **id** (INT): Plan id.
            - **week_number** (INT): Week Number.
            - **plan_description** (STR): Description of week plan.
            - **start_date** (DATE): Start date of week plan.
            - **end_date** (DATE): End date of week plan.
            - **current_start_month** (STR): Start month of week plan.
            - **current_end_month** (STR): End month of week plan.

    """
    print("Calling generate_general_udp method")

    result: GeneralUDPReadSchema = await udp_view.general_udp(
        db_session=db_session,
        current_user=current_user,
        subject_id=subject_id,
        start_month=start_month,
        end_month=end_month,
        status=status,
        sub_topic=sub_topic,
    )

    if not isinstance(result, GeneralUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={"detail": udp_response_message.SUBJECT_NOT_FOUND},
        )

    return GeneralUDPReadSchema.model_validate(obj=result)
