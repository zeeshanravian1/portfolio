"""
    Unit Delivery Plan Pydantic Schemas

    Description:
    - This module contains all udp schemas used by API.

"""

# Importing Python Packages
from datetime import date
from pydantic import BaseModel, Field

# Importing FastAPI Packages

# Importing Project Files
from .configuration import udp_configuration
from typing import List


# -----------------------------------------------------------------------------


class GeneralPlanSummarySchema(BaseModel):
    id: int
    grade: int
    start_date: str
    subject: str
    status: str


class GeneralPlanSummaryListSchema(BaseModel):
    general_plans: List[GeneralPlanSummarySchema]


class GeneralPlanSchema(BaseModel):
    id: int
    subject_id: int
    start_date: str
    status: str


class GeneralUDPDataUpdateSchema(BaseModel):
    week_number: int = None
    plan_description: str = None
    start_date: date = Field(example=udp_configuration.START_DATE)
    end_date: date = Field(example=udp_configuration.END_DATE)
    current_start_month: str = None
    current_end_month: str = None


class GeneralUDPDataSchema(BaseModel):
    """
    General Unit Delivery Plan Data Schema

    Description:
    - This schema is used to validate general udp data returned from API.

    """

    id: int = Field(ge=1, example=udp_configuration.ID)
    week_number: int = Field(ge=1, example=udp_configuration.WEEK_NUMBER)
    plan_description: str = Field(
        min_length=1,
        max_length=1_000,
        example=udp_configuration.PLAN_DESCRIPTION,
    )
    start_date: date = Field(example=udp_configuration.START_DATE)
    end_date: date = Field(example=udp_configuration.END_DATE)
    current_start_month: str = Field(
        min_length=1,
        max_length=2_0,
        example=udp_configuration.CURRENT_START_MONTH,
    )
    current_end_month: str = Field(
        min_length=1,
        max_length=2_0,
        example=udp_configuration.CURRENT_END_MONTH,
    )
    status: str = Field(
        min_length=1, max_length=2_0, example=udp_configuration.STATUS
    )

    class Config:
        """
        Pydantic Config

        Description:
        - This class is used to configure Pydantic schema.

        """

        str_strip_whitespace = True
        from_attributes = True


class GeneralUDPReadSchema(BaseModel):
    """
    General Unit Delivery Plan Read Schema

    Description:
    - This schema is used to validate general udp data returned from API.

    """

    subject_id: int = Field(ge=1, example=udp_configuration.SUBJECT_ID)
    start_month: str = Field(
        min_length=1, max_length=2_0, example=udp_configuration.START_MONTH
    )
    end_month: str = Field(
        min_length=1, max_length=2_0, example=udp_configuration.END_MONTH
    )
    unit_duration: str = Field(
        min_length=1, max_length=2_0, example=udp_configuration.UNIT_DURATION
    )
    class_size: int = Field(ge=0, example=udp_configuration.CLASS_SIZE)
    assessment_details: str = Field(
        min_length=1,
        max_length=10_000,
        example=udp_configuration.ASSESSMENT_DETAILS,
    )
    notes: str = Field(
        min_length=1, max_length=10_000, example=udp_configuration.NOTES
    )
    status: str = Field(
        min_length=1, max_length=2_0, example=udp_configuration.STATUS
    )
    sub_topic: str = Field(
        min_length=1, max_length=2_0, example=udp_configuration.SUB_TOPIC
    )
    general_udp: list[GeneralUDPDataSchema]

    class Config:
        """
        Pydantic Config

        Description:
        - This class is used to configure Pydantic schema.

        """

        str_strip_whitespace = True
        from_attributes = True
