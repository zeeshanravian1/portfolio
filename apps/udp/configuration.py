"""
    Unit Delivery Plan Configuration Module

    Description:
    - This module is responsible for udp configuration.

"""

# Importing Python Packages

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class UDPConfiguration:
    """
    Unit Delivery Plan Settings Class

    Description:
    - This class is used to define udp configurations.

    """

    ID: int = 1
    SUBJECT_ID: int = 1
    SCHOOL_TYPE: str = "Elementary/Secondary: Elementary"
    SCHOOL_LOCATION: str = "Riyadh,KSA"
    UNIT_DURATION: str = "6 months"
    START_MONTH: str = "February"
    END_MONTH: str = "July"
    CLASS_SIZE: int = 30
    WEEK_NUMBER: int = 1
    PLAN_DESCRIPTION: str = "Topic"
    START_DATE: str = "2021-02-01"
    END_DATE: str = "2021-02-07"
    CURRENT_START_MONTH: str = "February"
    CURRENT_END_MONTH: str = "February"
    ASSESSMENT_DETAILS: str = "Assessment details"
    NOTES: str = "Additional notes"
    STATUS: str = "Pending"
    SUB_TOPIC: str = "Algebra"


udp_configuration = UDPConfiguration()
