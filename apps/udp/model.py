"""
    General Unit Delivery Plan Models

    Description:
    - This file contains model for general plan table.

"""

# Importing Python Packages
from sqlalchemy import Integer, String, Date, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

# Importing FastAPI Packages

# Importing Project Files
from database import BaseTable
from apps.subject.model import SubjectTable
from apps.user.model import UserTable


# -----------------------------------------------------------------------------


class GeneralPlanTable(BaseTable):
    """
    General Plan Table

    Description:
    - This table is used to create general plan in database.

    """

    user_id: Mapped[int] = mapped_column(
        Integer, ForeignKey(UserTable.id), nullable=False
    )
    subject_id: Mapped[int] = mapped_column(
        Integer, ForeignKey(SubjectTable.id), nullable=False
    )
    week_number: Mapped[int] = mapped_column(Integer, nullable=False)
    plan_description: Mapped[str] = mapped_column(
        String(1_000), nullable=False
    )
    start_month: Mapped[str] = mapped_column(String(2_0), nullable=False)
    end_month: Mapped[str] = mapped_column(String(2_0), nullable=False)
    start_date: Mapped[Date] = mapped_column(Date, nullable=False)
    end_date: Mapped[Date] = mapped_column(Date, nullable=False)
    current_start_month: Mapped[str] = mapped_column(
        String(2_0), nullable=False
    )
    current_end_month: Mapped[str] = mapped_column(String(2_0), nullable=False)
    unit_duration: Mapped[str] = mapped_column(String(2_0), nullable=False)
    class_size: Mapped[int] = mapped_column(Integer, nullable=False)
    assessment_details: Mapped[str] = mapped_column(
        String(10_000), nullable=True
    )
    notes: Mapped[str] = mapped_column(String(10_000), nullable=True)
    status: Mapped[str] = mapped_column(
        String(2_0), nullable=False, server_default="Pending"
    )
    sub_topic: Mapped[str] = mapped_column(String(2_0), nullable=True)

    # Relationship
    generalplan_subject = relationship(
        "SubjectTable", back_populates="subject_generalplan", lazy="joined"
    )
    generalplan_weeklyplan = relationship(
        "WeeklyPlanTable", back_populates="weekly_plan_generalplan"
    )
