"""
    Unit Delivery Plan Response Message Module

    Description:
    - This module is responsible for udp response messages.

"""

# Importing Python Packages

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class UDPResponseMessage:
    """
    Unit Delivery Plan Response Message Class

    Description:
    - This class is used to define udp response messages.

    """

    SUBJECT_NOT_FOUND: str = "Subject not found"
    GENERAL_PLAN_NOT_FOUND: str = "General plan not found"


udp_response_message = UDPResponseMessage()
