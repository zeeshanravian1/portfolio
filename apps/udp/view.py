"""
    Unit Delivery Plan View Module

    Description:
    - This module generates unit delivery plan.

"""

# Importing Python Packages
from datetime import datetime, timedelta
import re
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.engine.result import ChunkedIteratorResult
from sqlalchemy.sql.selectable import Select
from langchain import LLMChain
from langchain.chat_models import ChatOpenAI
from langchain.prompts import PromptTemplate
from langchain.callbacks import get_openai_callback

# Importing FastAPI Packages

# Importing Project Files
from core.configuration import core_configuration
from core.schema import CurrentUserReadSchema
from apps.subject.model import SubjectTable
from .response_message import udp_response_message
from .model import GeneralPlanTable
from .schema import GeneralUDPDataSchema, GeneralUDPReadSchema


# -----------------------------------------------------------------------------


# GenerateUDP class
class GenerateUDP:
    """
    Generate Unit Delivery Plan Class

    Description:
    - This class is responsible for generating UDP.

    """

    llm = ChatOpenAI(
        temperature=0,
        openai_api_key=core_configuration.OPENAI_API_KEY,
        model_name=core_configuration.MODEL_NAME,
    )

    async def general_udp(
        self,
        db_session: AsyncSession,
        current_user: CurrentUserReadSchema,
        subject_id: int,
        sub_topic: str,
        start_month: str,
        end_month: str,
        status: str,
        class_size: int = 30,
        school_type: str = "Elementary",
        school_location: str = "Riyadh",
        unit_duration: str = "6 months",
        classroom_resources: str = "Math manipulatives, Whiteboard",
        learning_mode: str = "In-person Instruction",
        prior_knowledge: str = "Basic understanding of Mathematics",
        instructional_methods: str = "Hands-on Activities, Guided Practice",
        assessment_preferences: str = "Regular quizzes, Hands-on Projects, Oral Presentations",
        tech_tools: str = "Interactive Whiteboard, Math Games Apps",
        desired_outcomes: str = "Develop a solid foundation in Common Core Standrads concepts",
        review_frequency: str = "Weekly review sessions",
        stakeholders: str = "Teachers, Students, Parents",
        documentation_format: str = "Digital documents in PDF format",
        documentation_detail: str = "Detailed explanations and learning objectives included",
    ) -> GeneralUDPReadSchema:
        """
        General UDP

        Description:
        - This method is responsible for generating general udp.

        Parameter:
        - **subject_id** (INT): Id of Subject. **(Required)**
        - **school_type** (STR): Type of school. **(Required)**
        - **school_location** (STR): Location of school. **(Required)**
        - **unit_duration** (STR): Duration of unit. **(Required)**
        - **start_month** (STR): Starting month of unit. **(Required)**
        - **end_month** (STR): Ending month of unit. **(Required)**
        - **class_size** (INT): Number of students in class. **(Required)**
        - **classroom_resources** (STR): Classroom resources. **(Optional)**
        - **learning_mode** (STR): Learning mode. **(Optional)**
        - **prior_knowledge** (STR): Prior knowledge. **(Optional)**
        - **instructional_methods** (STR): Instructional methods.
        **(Optional)**
        - **assessment_preferences** (STR): Assessment preferences.
         **(Optional)**
        - **tech_tools** (STR): Tech tools. **(Optional)**
        - **desired_outcomes** (STR): Desired outcomes. **(Optional)**
        - **review_frequency** (STR): Review frequency. **(Optional)**
        - **stakeholders** (STR): Stakeholders. **(Optional)**
        - **documentation_format** (STR): Documentation format. **(Optional)**
        - **documentation_detail** (STR): Documentation detail. **(Optional)**

        Return:
        - **subject_id** (INT): Id of Subject.
        - **start_month** (STR): Starting month of unit.
        - **end_month** (STR): Ending month of unit.
        - **unit_duration** (STR): Duration of unit.
        - **class_size** (INT): Number of students in class.
        - **assessment_details** (STR): Generated assessment details.
        - **notes** (STR): Generated additional notes.
        - **general_udp** (LIST): Generated general udp.

        """

        # Check if subject exists
        query: Select = select(SubjectTable).where(
            SubjectTable.id == subject_id
        )
        result: ChunkedIteratorResult = await db_session.execute(
            statement=query
        )
        subject_result: SubjectTable = result.scalars().first()

        if not subject_result:
            return {"detail": udp_response_message.SUBJECT_NOT_FOUND}

        # Get current year
        current_year = datetime.now().year

        # Convert month abbreviations to full month names
        start_month_format = "%b" if len(start_month) <= 3 else "%B"
        end_month_format = "%b" if len(end_month) <= 3 else "%B"

        start_month = datetime.strptime(
            start_month, start_month_format
        ).strftime("%B")
        end_month = datetime.strptime(end_month, end_month_format).strftime(
            "%B"
        )

        response = {
            "subject_id": subject_id,
            "start_month": start_month,
            "end_month": end_month,
            "unit_duration": unit_duration,
            "class_size": class_size,
            "sub_topic": sub_topic,
            "assessment_details": None,
            "notes": None,
            "general_udp": [],
        }

        # Check if plan already generated
        query: Select = select(GeneralPlanTable).where(
            GeneralPlanTable.subject_id == subject_id,
            GeneralPlanTable.start_month == start_month,
            GeneralPlanTable.end_month == end_month,
        )
        result: ChunkedIteratorResult = await db_session.execute(
            statement=query
        )
        generalplan_result: list = result.scalars().all()

        if generalplan_result:
            return GeneralUDPReadSchema(
                subject_id=subject_id,
                start_month=start_month,
                end_month=end_month,
                unit_duration=unit_duration,
                class_size=class_size,
                status=status,
                sub_topic=sub_topic,
                assessment_details=generalplan_result[0].assessment_details,
                notes=generalplan_result[0].notes,
                general_udp=[
                    GeneralUDPDataSchema(
                        id=record.id,
                        week_number=record.week_number,
                        plan_description=record.plan_description,
                        start_date=record.start_date,
                        end_date=record.end_date,
                        current_start_month=record.current_start_month,
                        current_end_month=record.current_end_month,
                        status=record.status,
                    )
                    for record in generalplan_result
                ],
            )

        # Specifiying how General plan should be structured
        template = """You are an Educational Planner Chatbot creating a Unit Development Plan based on complete set of Common Core Standards with all the benchmarks for students in {grade_level} for the subject of {subject_area}.


Given the educational parameters provided, construct a Unit Delivery Plan (UDP) for students in {grade_level} for the subject of {subject_area} in the following structured format:


Unit Delivery Plan (UDP) for {school_type} in {school_location} using Common Core Standards.

Subject Area: {subject_area}

Grade Level: {grade_level}

Duration of the Unit: {unit_duration}

Starting month: {start_month}

End month: {end_month}

Class Size: {class_size}

Classroom Resources: {classroom_resources}

Learning Mode: {learning_mode}

Prior Knowledge Assumptions: {prior_knowledge}

Instructional Methods Preference: {instructional_methods}

Assessment Preferences: {assessment_preferences}

Available Tech Tools: {tech_tools}

Desired Outcomes Beyond Standards: {desired_outcomes}

Review Frequency: {review_frequency}

Stakeholders for Review: {stakeholders}

Documentation Format: {documentation_format}

Depth of Detail in Documentation: {documentation_detail}


Unit Overview:
Provide a narrative based on the Common Core Standards, benchmarks, topics, duration, and desired outcomes.

Month-by-Month Breakdown:
Organize the Benchmarks of Common Core Standards with correct sequence into monthly segments and detailing topics. Starting from {start_month} to {end_month}.

Month 1:
Week 1: Topic aligned with Common Core Standard: 3.OA.A.1
Week 2: Topic aligned with Common Core Standard: 3.OA.A.2
Week 3: Topic aligned with Common Core Standard: 3.OA.A.3
Week 4: Topic aligned with Common Core Standard: 3.OA.A.4

Month 2:
Week 5: Topic aligned with Common Core Standard: 3.OA.B.5
Week 6: Topic aligned with Common Core Standard: 3.OA.B.6
Week 7: Topic aligned with Common Core Standard: 3.OA.C.7
Week 8: Topic aligned with Common Core Standard: 3.OA.D.8

Month 3:
Week 9: Topic aligned with Common Core Standard: 3.OA.D.9
Week 10: Topic aligned with Common Core Standard: 3.NBT.A.1
Week 11: Topic aligned with Common Core Standard: 3.NBT.A.2
Week 12: Topic aligned with Common Core Standard: 3.NBT.A.3


Month 4:
Week 13: Topic aligned with Common Core Standard: 3.NF.A.1
Week 14: Topic aligned with Common Core Standard: 3.NF.A.2
Week 15: Topic aligned with Common Core Standard: 3.NF.A.3
Week 16: Topic aligned with Common Core Standard: 3.MD.A.1

Month 5:
Week 17: Topic aligned with Common Core Standard: 3.MD.A.2
Week 18: Topic aligned with Common Core Standard: 3.MD.B.3
Week 19: Topic aligned with Common Core Standard: 3.MD.B.4
Week 20: Topic aligned with Common Core Standard: 3.MD.C.5


Month 6:
Week 21: Topic aligned with Common Core Standard: 3.MD.C.6
Week 22: Topic aligned with Common Core Standard: 3.MD.C.7
Week 23: Topic aligned with Common Core Standard: 3.MD.D.8
Week 24: Topic aligned with Common Core Standard: 3.G.A.1
Week 25: Topic aligned with Common Core Standard: 3.G.A.2

Assessments:
Describe the formative and summative assessment strategies, integrating tech tools where applicable.

Additional Notes:
Include any other relevant information or considerations.

Educational Planner Chatbot:
                   """

        # Initializing prompt
        prompt = PromptTemplate(
            template=template,
            input_variables=[
                "grade_level",
                "subject_area",
                "school_type",
                "school_location",
                "unit_duration",
                "start_month",
                "end_month",
                "class_size",
                "classroom_resources",
                "learning_mode",
                "prior_knowledge",
                "instructional_methods",
                "assessment_preferences",
                "tech_tools",
                "desired_outcomes",
                "review_frequency",
                "stakeholders",
                "documentation_format",
                "documentation_detail",
            ],
        )

        # Initilaize LLM chain
        llm_chain = LLMChain(llm=self.llm, prompt=prompt)

        with get_openai_callback():
            result = llm_chain(
                {
                    "grade_level": subject_result.grade_level,
                    "subject_area": subject_result.subject_name,
                    "school_type": school_type,
                    "school_location": school_location,
                    "unit_duration": unit_duration,
                    "start_month": start_month,
                    "end_month": end_month,
                    "class_size": class_size,
                    "classroom_resources": classroom_resources,
                    "learning_mode": learning_mode,
                    "prior_knowledge": prior_knowledge,
                    "instructional_methods": instructional_methods,
                    "assessment_preferences": assessment_preferences,
                    "tech_tools": tech_tools,
                    "desired_outcomes": desired_outcomes,
                    "review_frequency": review_frequency,
                    "stakeholders": stakeholders,
                    "documentation_format": documentation_format,
                    "documentation_detail": documentation_detail,
                },
                return_only_outputs=True,
            )

            general_plan = result["text"]

            # Extract assessments
            pattern = re.compile(r"Assessments:\n(.+?)(?=\n)")
            match = pattern.search(general_plan)

            if match:
                response["assessment_details"] = match.group(1).strip()

            # Extract additional notes
            pattern = re.compile(r"Additional Notes:\n(.+)")
            match = pattern.search(general_plan)

            if match:
                response["notes"] = match.group(1).strip()

            # Extract plans
            pattern = re.compile(
                r"Week (\d+): Topic aligned with Common Core Standard: (.+)"
            )
            matches = pattern.findall(general_plan)

            for week, topic in matches:
                start_date = datetime(
                    current_year, datetime.strptime(start_month, "%B").month, 1
                ) + timedelta(weeks=int(week) - 1)
                end_date = start_date + timedelta(days=6)

                response["general_udp"].append(
                    {
                        "week_number": int(week),
                        "plan_description": topic,
                        "start_date": start_date.date(),
                        "end_date": end_date.date(),
                        "current_start_month": start_date.strftime("%B"),
                        "current_end_month": end_date.strftime("%B"),
                    }
                )

        # Insert into database
        inserted_record = [
            GeneralPlanTable(
                user_id=current_user.id,
                subject_id=subject_id,
                week_number=data["week_number"],
                plan_description=data["plan_description"],
                start_month=start_month,
                end_month=end_month,
                start_date=data["start_date"],
                end_date=data["end_date"],
                current_start_month=data["current_start_month"],
                current_end_month=data["current_end_month"],
                unit_duration=unit_duration,
                class_size=class_size,
                assessment_details=response["assessment_details"],
                notes=response["notes"],
                status=status,
                sub_topic=sub_topic,
            )
            for data in response.get("general_udp")
        ]

        db_session.add_all(instances=inserted_record)
        await db_session.commit()

        for record in inserted_record:
            await db_session.refresh(instance=record)

        return GeneralUDPReadSchema(
            subject_id=subject_id,
            start_month=start_month,
            end_month=end_month,
            unit_duration=unit_duration,
            class_size=class_size,
            sub_topic=sub_topic,
            status=status,
            assessment_details=response["assessment_details"],
            notes=response["notes"],
            general_udp=[
                GeneralUDPDataSchema(
                    id=record.id,
                    week_number=record.week_number,
                    plan_description=record.plan_description,
                    start_date=record.start_date,
                    end_date=record.end_date,
                    current_start_month=record.current_start_month,
                    current_end_month=record.current_end_month,
                    status=record.status,
                )
                for record in inserted_record
            ],
        )


udp_view = GenerateUDP()
