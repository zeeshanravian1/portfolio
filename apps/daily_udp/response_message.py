"""
    Daily Unit Delivery Plan Response Message Module

    Description:
    - This module is responsible for daily udp response messages.

"""

# Importing Python Packages

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class DailyUDPResponseMessage:
    """
    Daily Unit Delivery Plan Response Message Class

    Description:
    - This class is used to define daily udp response messages.

    """

    WEEKLY_PLAN_NOT_FOUND: str = "Weekly plan not found"
    DAILY_UDP_NOT_FOUND: str = "Daily udp not found"


daily_udp_response_message = DailyUDPResponseMessage()
