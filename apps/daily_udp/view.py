"""
    Daily Unit Delivery Plan View Module

    Description:
    - This module is responsible for daily udp views.

"""

# Importing Python Packages
from datetime import timedelta, date
import re
from sqlalchemy import select, update, delete
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.engine.result import ChunkedIteratorResult
from sqlalchemy.sql.selectable import Select
from sqlalchemy.sql.dml import Update, Delete
from langchain import LLMChain
from langchain.chat_models import ChatOpenAI
from langchain.prompts import PromptTemplate
from langchain.callbacks import get_openai_callback

# Importing FastAPI Packages

# Importing Project Files
from core.configuration import core_configuration
from core.schema import CurrentUserReadSchema
from apps.weekly_udp.model import WeeklyPlanTable
from .configuration import daily_udp_configuration
from .response_message import daily_udp_response_message
from .model import DailyPlanTable
from .schema import (
    DailyUDPDataSchema,
    DailyUDPReadSchema,
    DailyUDPPatchSchema,
)


# -----------------------------------------------------------------------------


# DailyUDP View class
class DailyUDPView:
    """
    Daily Unit Delivery Plan View Class

    Description:
    - This class is responsible for daily udp views.

    """

    llm = ChatOpenAI(
        temperature=0,
        openai_api_key=core_configuration.OPENAI_API_KEY,
        model_name=core_configuration.MODEL_NAME,
    )

    async def create(
        self,
        weeklyplan_id: int,
        db_session: AsyncSession,
        current_user: CurrentUserReadSchema,
    ) -> DailyUDPReadSchema:
        """
        Create a daily udp

        Description:
        - This method is responsible for creating daily udp.

        Parameter:
        - **weeklyplan_id** (INT): Id of weekly plan. **(Required)**
        - **db_session** (Session): Database session. **(Required)**
        - **current_user** (CurrentUserReadSchema): Current user details.
        **(Required)**

        Return:
        Daily udp details along with following information:
        - **weeklyplan_id** (INT): Id of weekly plan.
        - **plan_date** (DATE): Date of daily udp.
        - **plan_month** (STR): Month of daily udp.
        - **plan_day** (STR): Day of daily udp.
        - **objective** (LIST): List of objectives.
        - **morning_warmup** (LIST): List of morning warmup.
        - **main_lesson** (LIST): List of main lesson.
        - **independent_work** (LIST): List of independent work.
        - **closing_home_assignment** (LIST): List of closing home assignment.
        - **material_needed** (LIST): List of material needed.
        - **success_criteria_teacher** (LIST): List of success criteria
        teacher.
        - **success_criteria_student** (LIST): List of success criteria
        student.
        - **notes** (STR): Additional notes.
        - **daily_udp** (LIST): List of daily udp.
            - Each daily udp contains following information:
                - **id** (INT): Id of daily udp.
                - **plan_description** (STR): Description of daily udp.
                - **status** (STR): Status of daily udp.
                - **created_at** (DATETIME): Datetime of daily udp creation.
                - **updated_at** (DATETIME): Datetime of daily udp updation.

        """

        # Check if weekly plan exists
        query: Select = select(WeeklyPlanTable).where(
            WeeklyPlanTable.id == weeklyplan_id
        )
        result: ChunkedIteratorResult = await db_session.execute(
            statement=query
        )
        weeklyplan_result: WeeklyPlanTable = result.scalars().first()

        if not weeklyplan_result:
            return {"detail": daily_udp_response_message.WEEKLY_PLAN_NOT_FOUND}

        response = {
            "weeklyplan_id": weeklyplan_id,
            "plan_date": weeklyplan_result.plan_date,
            "plan_month": weeklyplan_result.plan_month,
            "plan_day": weeklyplan_result.plan_day,
            "objective": None,
            "morning_warmup": None,
            "main_lesson": None,
            "independent_work": None,
            "closing_home_assignment": None,
            "material_needed": None,
            "success_criteria_teacher": None,
            "success_criteria_student": None,
            "notes": [],
            "daily_udp": [],
        }

        # Specifiying how weekly plan should be structured
        template = """You are an Educational Planner Chatbot creating a Unit Development Plan based on complete set of Common Core Standards with all the benchmarks for students in {grade_level} for the subject of Mathematics.

Your goal is to construct a detailed lesson plan for lesson {lesson_title} in the following structured format:

Lesson Plan for {lesson_title} using Common Core Standards - {day}, {lesson_date}

Subtopics:
State the subtopics from the {lesson_title} arranged in numbers aligned with Common Core Standards

Objectives:
List the objectives for the day.

Morning Warm-Up:
Detail the morning activities.

Main Lesson:
Detail the main lesson activities.

Independent Work:
Detail the independent work activities.

Closing and Homework Assignment:
Detail the closing activities and homework assignments.

Materials Needed:
List the materials needed for the day.

Success Criteria for the Teacher:
List the success criteria for the teacher.

Success Criteria for the Students:
List the success criteria for the students.

Additional Notes:
Include any other relevant information or considerations for the day.

Educational Planner Chatbot:
                    """

        # Initializing prompt
        prompt = PromptTemplate(
            template=template,
            input_variables=[
                "grade_level",
                "lesson_title",
                "day",
                "lesson_date",
            ],
        )

        # Initilaize LLM chain
        llm_chain = LLMChain(llm=self.llm, prompt=prompt)

        with get_openai_callback():
            result = llm_chain(
                {
                    "grade_level": weeklyplan_result.weekly_plan_generalplan.generalplan_subject.grade_level,
                    "lesson_title": weeklyplan_result.plan_description,
                    "day": weeklyplan_result.plan_day,
                    "lesson_date": weeklyplan_result.plan_date,
                },
                return_only_outputs=True,
            )

            daily_plan = result["text"]

            response = {
                "weeklyplan_id": weeklyplan_id,
                "plan_date": weeklyplan_result.plan_date,
                "plan_month": weeklyplan_result.plan_month,
                "plan_day": weeklyplan_result.plan_day,
                "objective": None,
                "morning_warmup": None,
                "main_lesson": None,
                "independent_work": None,
                "closing_home_assignment": None,
                "material_needed": None,
                "success_criteria_teacher": None,
                "success_criteria_student": None,
                "notes": [],
                "daily_udp": [],
            }

            # Extract plan_description
            pattern = re.compile(
                pattern=r"Subtopics:\n.*?(?=\n\n)", flags=re.DOTALL
            )
            match = pattern.search(daily_plan)

            if match:
                pattern = re.compile(
                    pattern=daily_udp_configuration.INNER_PATTERN
                )
                matches = pattern.findall(match.group())

                response["daily_udp"] = [
                    data[1].strip() for data in matches[1:]
                ]

            # Extract Objective
            pattern = re.compile(
                pattern=r"Objectives:\n.*?(?=\n\n)", flags=re.DOTALL
            )
            match = pattern.search(daily_plan)

            if match:
                pattern = re.compile(
                    pattern=daily_udp_configuration.INNER_PATTERN
                )
                matches = pattern.findall(match.group())

                response["objective"] = [
                    data[1].strip() for data in matches[1:]
                ]

            # Extract Morning Warm-Up
            pattern = re.compile(
                pattern=r"Morning Warm-Up:\n.*?(?=\n\n)", flags=re.DOTALL
            )
            match = pattern.search(daily_plan)

            if match:
                pattern = re.compile(
                    pattern=daily_udp_configuration.INNER_PATTERN
                )
                matches = pattern.findall(match.group())

                response["morning_warmup"] = [
                    data[1].strip() for data in matches[1:]
                ]

            # Extract Main Lesson
            pattern = re.compile(
                pattern=r"Main Lesson:\n.*?(?=\n\n)", flags=re.DOTALL
            )
            match = pattern.search(daily_plan)

            if match:
                pattern = re.compile(
                    pattern=daily_udp_configuration.INNER_PATTERN
                )
                matches = pattern.findall(match.group())

                response["main_lesson"] = [
                    data[1].strip() for data in matches[1:]
                ]

            # Extract Independent Work
            pattern = re.compile(
                pattern=r"Independent Work:\n.*?(?=\n\n)", flags=re.DOTALL
            )
            match = pattern.search(daily_plan)

            if match:
                pattern = re.compile(
                    pattern=daily_udp_configuration.INNER_PATTERN
                )
                matches = pattern.findall(match.group())

                response["independent_work"] = [
                    data[1].strip() for data in matches[1:]
                ]

            # Extract Closing and Homework Assignment
            pattern = re.compile(
                pattern=r"Closing and Homework Assignment:\n.*?(?=\n\n)",
                flags=re.DOTALL,
            )
            match = pattern.search(daily_plan)

            if match:
                pattern = re.compile(
                    pattern=daily_udp_configuration.INNER_PATTERN
                )
                matches = pattern.findall(match.group())

                response["closing_home_assignment"] = [
                    data[1].strip() for data in matches[1:]
                ]

            # Extract Materials Needed
            pattern = re.compile(
                pattern=r"Materials Needed:\n.*?(?=\n\n)", flags=re.DOTALL
            )
            match = pattern.search(daily_plan)

            if match:
                pattern = re.compile(
                    pattern=daily_udp_configuration.INNER_PATTERN
                )
                matches = pattern.findall(match.group())

                response["material_needed"] = [
                    data[1].strip() for data in matches[1:]
                ]

            # Extract Success Criteria for the Teacher
            pattern = re.compile(
                pattern=r"Success Criteria for the Teacher:\n.*?(?=\n\n)",
                flags=re.DOTALL,
            )
            match = pattern.search(daily_plan)

            if match:
                pattern = re.compile(
                    pattern=daily_udp_configuration.INNER_PATTERN
                )
                matches = pattern.findall(match.group())

                response["success_criteria_teacher"] = [
                    data[1].strip() for data in matches[1:]
                ]

            # Extract Success Criteria for the Students
            pattern = re.compile(
                pattern=r"Success Criteria for the Students:\n.*?(?=\n\n)",
                flags=re.DOTALL,
            )
            match = pattern.search(daily_plan)

            if match:
                pattern = re.compile(
                    pattern=daily_udp_configuration.INNER_PATTERN
                )
                matches = pattern.findall(match.group())

                response["success_criteria_student"] = [
                    data[1].strip() for data in matches[1:]
                ]

            # Extract Additional Notes
            pattern = re.compile(
                pattern=r"(Note|Notes|Additional Note|Additional Notes):\n.*",
                flags=re.DOTALL,
            )
            match = pattern.search(daily_plan)

            if match:
                pattern = re.compile(pattern=r"(-|\d.|\w.)(.+)")
                matches = pattern.findall(match.group())

                response["notes"] = [data[1].strip() for data in matches[1:]]

        # Insert into database
        inserted_record = [
            DailyPlanTable(
                user_id=current_user.id,
                weeklyplan_id=weeklyplan_id,
                plan_date=response["plan_date"],
                plan_month=response["plan_month"],
                plan_day=response["plan_day"],
                plan_description=data,
                objective=response["objective"],
                morning_warmup=response["morning_warmup"],
                main_lesson=response["main_lesson"],
                independent_work=response["independent_work"],
                closing_home_assignment=response["closing_home_assignment"],
                material_needed=response["material_needed"],
                success_criteria_teacher=response["success_criteria_teacher"],
                success_criteria_student=response["success_criteria_student"],
                notes=response["notes"],
            )
            for data in response.get("daily_udp")
        ]

        db_session.add_all(instances=inserted_record)
        await db_session.commit()

        for record in inserted_record:
            await db_session.refresh(instance=record)

        return DailyUDPReadSchema(
            weeklyplan_id=weeklyplan_id,
            plan_date=response["plan_date"],
            plan_month=response["plan_month"],
            plan_day=response["plan_day"],
            objective=response["objective"],
            morning_warmup=response["morning_warmup"],
            main_lesson=response["main_lesson"],
            independent_work=response["independent_work"],
            closing_home_assignment=response["closing_home_assignment"],
            material_needed=response["material_needed"],
            success_criteria_teacher=response["success_criteria_teacher"],
            success_criteria_student=response["success_criteria_student"],
            notes=response["notes"],
            daily_udp=[
                DailyUDPDataSchema(
                    id=record.id,
                    plan_description=record.plan_description,
                    status=record.status,
                    created_at=record.created_at,
                    updated_at=record.updated_at,
                )
                for record in inserted_record
            ],
        )

    async def read_by_id(
        self,
        record_id: int,
        column_name: str,
        db_session: AsyncSession,
        current_user: CurrentUserReadSchema,
    ) -> DailyUDPReadSchema:
        """
        Get a single daily udp.

        Description:
        - This method is responsible for reading a record by ID.

        Parameter:
        - **record_id** (INT): ID of daily udp to be fetched. **(Required)**
        - **column_name** (STR): Column name. **(Required)**
        - **db_session** (Session): Database session. **(Required)**
        - **current_user** (CurrentUserReadSchema): Current user details.
        **(Required)**

        Return:
        Daily udp details along with following information:
        - **weeklyplan_id** (INT): Id of weekly plan.
        - **plan_date** (DATE): Date of daily udp.
        - **plan_month** (STR): Month of daily udp.
        - **plan_day** (STR): Day of daily udp.
        - **objective** (LIST): List of objectives.
        - **morning_warmup** (LIST): List of morning warmup.
        - **main_lesson** (LIST): List of main lesson.
        - **independent_work** (LIST): List of independent work.
        - **closing_home_assignment** (LIST): List of closing home assignment.
        - **material_needed** (LIST): List of material needed.
        - **success_criteria_teacher** (LIST): List of success criteria
        teacher.
        - **success_criteria_student** (LIST): List of success criteria
        student.
        - **notes** (STR): Additional notes.
        - **daily_udp** (LIST): List of daily udp.
            - Each daily udp contains following information:
                - **id** (INT): Id of daily udp.
                - **plan_description** (STR): Description of daily udp.
                - **status** (STR): Status of daily udp.
                - **created_at** (DATETIME): Datetime of daily udp creation.
                - **updated_at** (DATETIME): Datetime of daily udp updation.

        """

        query: Select = select(DailyPlanTable).where(
            getattr(DailyPlanTable, column_name) == record_id,
            DailyPlanTable.user_id == current_user.id,
        )
        result: ChunkedIteratorResult = await db_session.execute(
            statement=query
        )
        dailyplan_result: list = result.scalars().all()

        if not dailyplan_result:
            return {"detail": daily_udp_response_message.DAILY_UDP_NOT_FOUND}

        return DailyUDPReadSchema(
            weeklyplan_id=dailyplan_result[0].weeklyplan_id,
            plan_date=dailyplan_result[0].plan_date,
            plan_month=dailyplan_result[0].plan_month,
            plan_day=dailyplan_result[0].plan_day,
            objective=dailyplan_result[0].objective,
            morning_warmup=dailyplan_result[0].morning_warmup,
            main_lesson=dailyplan_result[0].main_lesson,
            independent_work=dailyplan_result[0].independent_work,
            closing_home_assignment=dailyplan_result[
                0
            ].closing_home_assignment,
            material_needed=dailyplan_result[0].material_needed,
            success_criteria_teacher=dailyplan_result[
                0
            ].success_criteria_teacher,
            success_criteria_student=dailyplan_result[
                0
            ].success_criteria_student,
            notes=dailyplan_result[0].notes,
            daily_udp=[
                DailyUDPDataSchema(
                    id=record.id,
                    plan_description=record.plan_description,
                    status=record.status,
                    created_at=record.created_at,
                    updated_at=record.updated_at,
                )
                for record in dailyplan_result
            ],
        )

    async def update(
        self,
        record_id: int,
        record: DailyUDPPatchSchema,
        db_session: AsyncSession,
        current_user: CurrentUserReadSchema,
    ) -> DailyUDPReadSchema:
        """
        Update a single daily udp.

        Description:
        - This method is responsible for updaing a record by ID.

        Parameter:
        - **record_id** (INT): ID of daily udp to be update. **(Required)**
        - **db_session** (Session): Database session. **(Required)**
        - **current_user** (CurrentUserReadSchema): Current user details.
        **(Required)**
        - **plan_date** (DATE): Date of daily udp. **(Optional)**
        - **plan_month** (STR): Month of daily plan. **(Optional)**
        - **plan_day** (STR): Day of daily plan. **(Optional)**
        - **plan_description** (STR): Description of daily udp. **(Optional)**
        - **objective** (LIST): List of objectives. **(Optional)**
        - **morning_warmup** (LIST): List of morning warmup. **(Optional)**
        - **main_lesson** (LIST): List of main lesson. **(Optional)**
        - **independent_work** (LIST): List of independent work. **(Optional)**
        - **closing_home_assignment** (LIST): List of closing home assignment.
        **(Optional)**
        - **material_needed** (LIST): List of material needed. **(Optional)**
        - **success_criteria_teacher** (LIST): List of success criteria
        teacher. **(Optional)**
        - **success_criteria_student** (LIST): List of success criteria
        student. **(Optional)**
        - **notes** (STR): Additional notes. **(Optional)**
        - **status** (STR): Status of daily udp. **(Optional)**

        Return:
        Weekly udp details along with following information:
        - **generalplan_id** (INT): Id of general plan.
        - **notes** (STR): Additional notes.
        - **weekly_udp** (LIST): List of weekly udp.
            - Each weekly udp contains following information:
                - **id** (INT): Id of weekly udp.
                - **plan_date** (DATE): Date of weekly udp.
                - **plan_month** (STR): Month of weekly udp.
                - **plan_day** (STR): Day of weekly udp.
                - **plan_description** (STR): Description of weekly udp.
                - **sub_plans_description** (LIST): List of sub plans
                description.
                - **status** (STR): Status of weekly udp.
                - **created_at** (DATETIME): Datetime of weekly udp creation.
                - **updated_at** (DATETIME): Datetime of weekly udp updation.

        """

        query: Update = (
            update(DailyPlanTable)
            .where(DailyPlanTable.id == record_id)
            .values(record.model_dump(exclude_unset=True))
        )
        await db_session.execute(statement=query)
        await db_session.commit()

        return await self.read_by_id(
            record_id=record_id,
            column_name=daily_udp_configuration.ID_COLUMN_NAME,
            db_session=db_session,
            current_user=current_user,
        )

    async def delete(
        self,
        record_id: int,
        column_name: str,
        db_session: AsyncSession,
        current_user: CurrentUserReadSchema,
    ) -> DailyPlanTable:
        """
        Delete Method

        Description:
        - This method is responsible for deleting a record.

        Parameter:
        - **record_id** (int): Record ID. **(Required)**
        - **column_name** (STR): Column name. **(Required)**
        - **db_session** (Session): Database session. **(Required)**
        - **current_user** (CurrentUserReadSchema): Current user details.
        **(Required)**

        Return:
        - **record** (DailyPlanTable): SqlAlchemy Model Object.

        """

        result: DailyPlanTable = await self.read_by_id(
            record_id=record_id,
            column_name=column_name,
            db_session=db_session,
            current_user=current_user,
        )

        if not isinstance(result, DailyUDPReadSchema):
            return result

        query: Delete = delete(DailyPlanTable).where(
            getattr(DailyPlanTable, column_name) == record_id,
        )
        await db_session.execute(statement=query)
        await db_session.commit()

        return result


daily_udp_view = DailyUDPView()
