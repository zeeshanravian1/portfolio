"""
    Daily Unit Delivery Plan Models

    Description:
    - This file contains model for daily plan table.

"""

# Importing Python Packages
from sqlalchemy import ARRAY, Integer, String, Date, ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

# Importing FastAPI Packages

# Importing Project Files
from database import BaseTable
from apps.weekly_udp.model import WeeklyPlanTable
from apps.user.model import UserTable


# -----------------------------------------------------------------------------


class DailyPlanTable(BaseTable):
    """
    Daily Plan Table

    Description:
    - This table is used to create daily plan in database.

    """

    user_id: Mapped[int] = mapped_column(
        Integer, ForeignKey(UserTable.id), nullable=False
    )
    weeklyplan_id: Mapped[int] = mapped_column(
        Integer, ForeignKey(WeeklyPlanTable.id), nullable=False
    )
    plan_date: Mapped[Date] = mapped_column(Date, nullable=False)
    plan_month: Mapped[str] = mapped_column(String(2_0), nullable=False)
    plan_day: Mapped[str] = mapped_column(String(2_0), nullable=False)
    plan_description: Mapped[str] = mapped_column(
        String(1_000), nullable=False
    )
    objective: Mapped[ARRAY[str]] = mapped_column(
        ARRAY(String(10_000)), nullable=True
    )
    morning_warmup: Mapped[ARRAY[str]] = mapped_column(
        ARRAY(String(10_000)), nullable=True
    )
    main_lesson: Mapped[ARRAY[str]] = mapped_column(
        ARRAY(String(65_535)), nullable=True
    )
    independent_work: Mapped[ARRAY[str]] = mapped_column(
        ARRAY(String(10_000)), nullable=True
    )
    closing_home_assignment: Mapped[ARRAY[str]] = mapped_column(
        ARRAY(String(10_000)), nullable=True
    )
    material_needed: Mapped[ARRAY[str]] = mapped_column(
        ARRAY(String(10_000)), nullable=True
    )
    success_criteria_teacher: Mapped[ARRAY[str]] = mapped_column(
        ARRAY(String(10_000)), nullable=True
    )
    success_criteria_student: Mapped[ARRAY[str]] = mapped_column(
        ARRAY(String(10_000)), nullable=True
    )
    notes: Mapped[ARRAY[str]] = mapped_column(
        ARRAY(String(10_000)), nullable=True
    )
    status: Mapped[str] = mapped_column(
        String(2_0), nullable=False, server_default="pending"
    )

    # Relationship
    daily_plan_weeklyplan = relationship(
        "WeeklyPlanTable",
        back_populates="weeklyplan_dailyplan",
        lazy="joined",
    )
