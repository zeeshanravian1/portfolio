"""
    Daily Unit Delivery Plan Configuration Module

    Description:
    - This module is responsible for daily udp configuration.

"""

# Importing Python Packages
from datetime import date

# Importing FastAPI Packages

# Importing Project Files


# -----------------------------------------------------------------------------


class DailyUDPConfiguration:
    """
    Daily Unit Delivery Plan Settings Class

    Description:
    - This class is used to define daily udp configurations.

    """

    ID: int = 1
    PLAN_DATE: date = date.today()
    PLAN_MONTH: str = PLAN_DATE.strftime("%B")
    PLAN_DAY: str = PLAN_DATE.strftime("%A")
    PLAN_DESCRIPTION: str = "Plan Description"
    OBJECTIVE: list[str] = ["Objective 1", "Objective 2"]
    MORNING_WARMUP: list[str] = ["Morning Warmup 1", "Morning Warmup 2"]
    MAIN_LESSON: list[str] = ["Main Lesson 1", "Main Lesson 2"]
    INDEPENDENT_WORK: list[str] = ["Independent Work 1", "Independent Work 2"]
    CLOSING_HOME_ASSIGNMENT: list[str] = [
        "Closing Home Assignment 1",
        "Closing Home Assignment 2",
    ]
    MATERIAL_NEEDED: list[str] = ["Material Needed 1", "Material Needed 2"]
    SUCCESS_CRITERIA_TEACHER: list[str] = [
        "Success Criteria Teacher 1",
        "Success Criteria Teacher 2",
    ]
    SUCCESS_CRITERIA_STUDENT: list[str] = [
        "Success Criteria Student 1",
        "Success Criteria Student 2",
    ]
    NOTES: str = "Additional notes"
    STATUS: str = "Pending"

    INNER_PATTERN: str = r"(-|\d.|\w.)(.+?)(?=\n)"

    ID_COLUMN_NAME: str = "id"
    WEEKLY_PLAN_ID_COLUMN_NAME: str = "weeklyplan_id"


daily_udp_configuration = DailyUDPConfiguration()
