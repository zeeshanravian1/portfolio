"""
    Daily Unit Delivery Plan Route Module

    Description:
    - This module is responsible for handling daily udp routes.
    - It is used to create, get, update, delete daily udp details.

"""

# Importing Python Packages
from sqlalchemy.ext.asyncio import AsyncSession

# Importing FastAPI Packages
from fastapi import APIRouter, Depends, Security, status
from fastapi.responses import JSONResponse

# Importing Project Files
from database.session import get_session
from core.security import get_current_user
from core.schema import CurrentUserReadSchema
from .configuration import daily_udp_configuration
from .response_message import daily_udp_response_message
from .model import DailyPlanTable
from .schema import DailyUDPReadSchema, DailyUDPPatchSchema
from .view import daily_udp_view

# Router Object to Create Routes
router = APIRouter(prefix="/daily-udp", tags=["Daily UDP"])


# -----------------------------------------------------------------------------


# Create daily udp
@router.post(
    path="/{weeklyplan_id}/",
    status_code=status.HTTP_201_CREATED,
    summary="Create a daily udp",
    response_description="Daily udp created successfully",
)
async def create_daily_udp(
    weeklyplan_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> DailyUDPReadSchema:
    """
    Create a daily udp

    Description:
    - This route is used to create a daily udp.

    Parameter:
    - **weeklyplan_id** (INT): Id of weekly plan. **(Required)**

    Return:
    Daily udp details along with following information:
    - **weeklyplan_id** (INT): Id of weekly plan.
    - **plan_date** (DATE): Date of daily udp.
    - **plan_month** (STR): Month of daily udp.
    - **plan_day** (STR): Day of daily udp.
    - **objective** (LIST): List of objectives.
    - **morning_warmup** (LIST): List of morning warmup.
    - **main_lesson** (LIST): List of main lesson.
    - **independent_work** (LIST): List of independent work.
    - **closing_home_assignment** (LIST): List of closing home assignment.
    - **material_needed** (LIST): List of material needed.
    - **success_criteria_teacher** (LIST): List of success criteria teacher.
    - **success_criteria_student** (LIST): List of success criteria student.
    - **notes** (STR): Additional notes.
    - **daily_udp** (LIST): List of daily udp.
        - Each daily udp contains following information:
            - **id** (INT): Id of daily udp.
            - **plan_description** (STR): Description of daily udp.
            - **status** (STR): Status of daily udp.
            - **created_at** (DATETIME): Datetime of daily udp creation.
            - **updated_at** (DATETIME): Datetime of daily udp updation.

    """
    print("Calling create_daily_udp method")

    result: DailyUDPReadSchema = await daily_udp_view.create(
        weeklyplan_id=weeklyplan_id,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, DailyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "detail": daily_udp_response_message.WEEKLY_PLAN_NOT_FOUND
            },
        )

    return DailyUDPReadSchema.model_validate(obj=result)


# Get daily udp by id route
@router.get(
    path="/{daily_udp_id}/",
    status_code=status.HTTP_200_OK,
    summary="Get a single daily udp by providing id",
    response_description="Daily udp details fetched successfully",
)
async def get_daily_udp_by_id(
    daily_udp_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> DailyUDPReadSchema:
    """
    Get a single daily udp.

    Description:
    - This route is used to get a single daily udp by providing id.

    Parameter:
    - **daily_udp_id** (INT): ID of daily udp to be fetched. **(Required)**

    Return:
    Get a single daily udp with following information:
    - **weeklyplan_id** (INT): Id of weekly plan.
    - **plan_date** (DATE): Date of daily udp.
    - **plan_month** (STR): Month of daily udp.
    - **plan_day** (STR): Day of daily udp.
    - **objective** (LIST): List of objectives.
    - **morning_warmup** (LIST): List of morning warmup.
    - **main_lesson** (LIST): List of main lesson.
    - **independent_work** (LIST): List of independent work.
    - **closing_home_assignment** (LIST): List of closing home assignment.
    - **material_needed** (LIST): List of material needed.
    - **success_criteria_teacher** (LIST): List of success criteria teacher.
    - **success_criteria_student** (LIST): List of success criteria student.
    - **notes** (STR): Additional notes.
    - **daily_udp** (LIST): List of daily udp.
        - Each daily udp contains following information:
            - **id** (INT): Id of daily udp.
            - **plan_description** (STR): Description of daily udp.
            - **status** (STR): Status of daily udp.
            - **created_at** (DATETIME): Datetime of daily udp creation.
            - **updated_at** (DATETIME): Datetime of daily udp updation.

    """
    print("Calling get_weekly_udp_by_id method")

    result: DailyUDPReadSchema = await daily_udp_view.read_by_id(
        record_id=daily_udp_id,
        column_name=daily_udp_configuration.ID_COLUMN_NAME,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, DailyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={"detail": daily_udp_response_message.DAILY_UDP_NOT_FOUND},
        )

    return DailyUDPReadSchema.model_validate(obj=result)


# Get all daily udp by generalplan id route
@router.get(
    path="/all/{weeklyplan_id}/",
    status_code=status.HTTP_200_OK,
    summary="Get all daily udp by providing generalplan id",
    response_description="All daily udps fetched successfully",
)
async def get_all_daily_udp_by_weeklyplan_id(
    weeklyplan_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> DailyUDPReadSchema:
    """
    Get all daily udp by generalplan id.

    Description:
    - This route is used to get a single daily udp by providing id.

    Parameter:
    - **generalplan_id** (INT): ID of general udp to be fetched. **(Required)**

    Return:
    Get all daily udp with following information:
    - **weeklyplan_id** (INT): Id of weekly plan.
    - **plan_date** (DATE): Date of daily udp.
    - **plan_month** (STR): Month of daily udp.
    - **plan_day** (STR): Day of daily udp.
    - **objective** (LIST): List of objectives.
    - **morning_warmup** (LIST): List of morning warmup.
    - **main_lesson** (LIST): List of main lesson.
    - **independent_work** (LIST): List of independent work.
    - **closing_home_assignment** (LIST): List of closing home assignment.
    - **material_needed** (LIST): List of material needed.
    - **success_criteria_teacher** (LIST): List of success criteria teacher.
    - **success_criteria_student** (LIST): List of success criteria student.
    - **notes** (STR): Additional notes.
    - **daily_udp** (LIST): List of daily udp.
        - Each daily udp contains following information:
            - **id** (INT): Id of daily udp.
            - **plan_description** (STR): Description of daily udp.
            - **status** (STR): Status of daily udp.
            - **created_at** (DATETIME): Datetime of daily udp creation.
            - **updated_at** (DATETIME): Datetime of daily udp updation.

    """
    print("Calling get_all_daily_udp_by_weeklyplan_id method")

    result: DailyUDPReadSchema = await daily_udp_view.read_by_id(
        record_id=weeklyplan_id,
        column_name=daily_udp_configuration.WEEKLY_PLAN_ID_COLUMN_NAME,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, DailyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "detail": daily_udp_response_message.WEEKLY_PLAN_NOT_FOUND
            },
        )

    return DailyUDPReadSchema.model_validate(obj=result)


# Partial update a single daily udp route
@router.patch(
    path="/{daily_udp_id}/",
    status_code=status.HTTP_202_ACCEPTED,
    summary="Partially update a single daily udp by providing id",
    response_description="Daily udp partial updated successfully",
)
async def partial_update_daily_udp(
    daily_udp_id: int,
    record: DailyUDPPatchSchema,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> DailyUDPReadSchema:
    """
    Partial update a single daily udp.

    Description:
    - This route is used to partial update a single daily udp by providing id.

    Parameter:
    - **daily_udp_id** (INT): Id of daily udp. **(Required)**
    - **plan_date** (DATE): Date of daily udp. **(Optional)**
    - **plan_month** (STR): Month of daily plan. **(Optional)**
    - **plan_day** (STR): Day of daily plan. **(Optional)**
    - **plan_description** (STR): Description of daily udp. **(Optional)**
    - **objective** (LIST): List of objectives. **(Optional)**
    - **morning_warmup** (LIST): List of morning warmup. **(Optional)**
    - **main_lesson** (LIST): List of main lesson. **(Optional)**
    - **independent_work** (LIST): List of independent work. **(Optional)**
    - **closing_home_assignment** (LIST): List of closing home assignment.
    **(Optional)**
    - **material_needed** (LIST): List of material needed. **(Optional)**
    - **success_criteria_teacher** (LIST): List of success criteria teacher.
    **(Optional)**
    - **success_criteria_student** (LIST): List of success criteria student.
    **(Optional)**
    - **notes** (STR): Additional notes. **(Optional)**
    - **status** (STR): Status of daily udp. **(Optional)**

    Return:
    Daily udp details along with following information:
    - **weeklyplan_id** (INT): Id of weekly plan.
    - **plan_date** (DATE): Date of daily udp.
    - **plan_month** (STR): Month of daily udp.
    - **plan_day** (STR): Day of daily udp.
    - **objective** (LIST): List of objectives.
    - **morning_warmup** (LIST): List of morning warmup.
    - **main_lesson** (LIST): List of main lesson.
    - **independent_work** (LIST): List of independent work.
    - **closing_home_assignment** (LIST): List of closing home assignment.
    - **material_needed** (LIST): List of material needed.
    - **success_criteria_teacher** (LIST): List of success criteria teacher.
    - **success_criteria_student** (LIST): List of success criteria student.
    - **notes** (STR): Additional notes.
    - **daily_udp** (LIST): List of daily udp.
        - Each daily udp contains following information:
            - **id** (INT): Id of daily udp.
            - **plan_description** (STR): Description of daily udp.
            - **status** (STR): Status of daily udp.
            - **created_at** (DATETIME): Datetime of daily udp creation.
            - **updated_at** (DATETIME): Datetime of daily udp updation.

    """
    print("Calling partial_update_daily_udp method")

    result: DailyUDPReadSchema = await daily_udp_view.update(
        record_id=daily_udp_id,
        record=record,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, DailyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={"detail": daily_udp_response_message.DAILY_UDP_NOT_FOUND},
        )

    return DailyUDPReadSchema.model_validate(obj=result)


# Delete a single daily udp by id route
@router.delete(
    path="/{daily_udp_id}/",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete a single daily udp by providing id",
    response_description="Daily udp deleted successfully",
)
async def delete_daily_udp(
    daily_udp_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> None:
    """
    Delete a single daily udp.

    Description:
    - This route is used to delete a single daily udp by providing id.

    Parameter:
    - **daily_udp_id** (INT): ID of daily udp to be deleted. **(Required)**

    Return:
    - **None**

    """
    print("Calling delete_daily_udp method")

    result: DailyPlanTable = await daily_udp_view.delete(
        record_id=daily_udp_id,
        column_name=daily_udp_configuration.ID_COLUMN_NAME,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, DailyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={"detail": daily_udp_response_message.DAILY_UDP_NOT_FOUND},
        )


# Delete all daily udps by generalplan id route
@router.delete(
    path="/all/{weeklyplan_id}/",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete all daily udps by providing id",
    response_description="Daily udps deleted successfully",
)
async def delete_dall_daily_udp(
    weeklyplan_id: int,
    db_session: AsyncSession = Depends(get_session),
    current_user: CurrentUserReadSchema = Security(get_current_user),
) -> None:
    """
    Delete all daily udps.

    Description:
    - This route is used to delete all daily udps by providing weekly plan id.

    Parameter:
    - **weeklyplan_id** (INT): ID of weekly plan to be deleted.
    **(Required)**

    Return:
    - **None**

    """
    print("Calling delete_dall_daily_udp method")

    result: DailyPlanTable = await daily_udp_view.delete(
        record_id=weeklyplan_id,
        column_name=daily_udp_configuration.WEEKLY_PLAN_ID_COLUMN_NAME,
        db_session=db_session,
        current_user=current_user,
    )

    if not isinstance(result, DailyUDPReadSchema):
        return JSONResponse(
            status_code=status.HTTP_404_NOT_FOUND,
            content={
                "detail": daily_udp_response_message.WEEKLY_PLAN_NOT_FOUND
            },
        )
