"""
    Daily Unit Delivery Plan Pydantic Schemas

    Description:
    - This module contains all daily udp schemas used by API.

"""

# Importing Python Packages
from datetime import date
from pydantic import BaseModel, Field
from pydantic_settings import SettingsConfigDict

# Importing FastAPI Packages

# Importing Project Files
from apps.base import BaseReadSchema
from .configuration import daily_udp_configuration


# -----------------------------------------------------------------------------


class DailyUDPDataSchema(BaseReadSchema, BaseModel):
    """
    Daily Unit Delivery Plan Data Schema

    Description:
    - This schema is used to validate daily udp data returned from API.

    """

    plan_description: str | None = Field(
        default=None,
        min_length=1,
        max_length=1_000,
        example=daily_udp_configuration.PLAN_DESCRIPTION,
    )
    status: str | None = Field(
        default=None,
        min_length=1,
        max_length=2_0,
        example=daily_udp_configuration.STATUS,
    )

    # CoreConfiguration Configuration
    model_config = SettingsConfigDict(
        str_strip_whitespace=True, from_attributes=True
    )


class DailyUDPReadSchema(BaseModel):
    """
    Daily Unit Delivery Plan Read Schema

    Description:
    - This schema is used to validate daily udp returned from API.

    """

    weeklyplan_id: int = Field(ge=1, example=daily_udp_configuration.ID)
    plan_date: date = Field(example=daily_udp_configuration.PLAN_DATE)
    plan_month: str = Field(example=daily_udp_configuration.PLAN_MONTH)
    plan_day: str = Field(example=daily_udp_configuration.PLAN_DAY)
    objective: list[str] = Field(example=daily_udp_configuration.OBJECTIVE)
    morning_warmup: list[str] = Field(
        example=daily_udp_configuration.MORNING_WARMUP
    )
    main_lesson: list[str] = Field(example=daily_udp_configuration.MAIN_LESSON)
    independent_work: list[str] = Field(
        example=daily_udp_configuration.INDEPENDENT_WORK
    )
    closing_home_assignment: list[str] = Field(
        example=daily_udp_configuration.CLOSING_HOME_ASSIGNMENT
    )
    material_needed: list[str] = Field(
        example=daily_udp_configuration.MATERIAL_NEEDED
    )
    success_criteria_teacher: list[str] = Field(
        example=daily_udp_configuration.SUCCESS_CRITERIA_TEACHER
    )
    success_criteria_student: list[str] = Field(
        example=daily_udp_configuration.SUCCESS_CRITERIA_STUDENT
    )
    notes: list[str] | None = Field(
        example=daily_udp_configuration.NOTES,
    )
    daily_udp: list[DailyUDPDataSchema]

    # CoreConfiguration Configuration
    model_config = SettingsConfigDict(
        str_strip_whitespace=True, from_attributes=True
    )


class DailyUDPPatchSchema(BaseModel):
    """
    Daily Unit Delivery Plan patch Schema

    Description:
    - This schema is used to validate daily udp passed to API.

    """

    plan_date: date | None = Field(
        default=None, example=daily_udp_configuration.PLAN_DATE
    )
    plan_month: str | None = Field(
        default=None, example=daily_udp_configuration.PLAN_MONTH
    )
    plan_day: str | None = Field(
        default=None, example=daily_udp_configuration.PLAN_DAY
    )
    plan_description: str | None = Field(
        default=None,
        min_length=1,
        max_length=1_000,
        example=daily_udp_configuration.PLAN_DESCRIPTION,
    )
    objective: list[str] | None = Field(
        default=None, example=daily_udp_configuration.OBJECTIVE
    )
    morning_warmup: list[str] | None = Field(
        default=None, example=daily_udp_configuration.MORNING_WARMUP
    )
    main_lesson: list[str] | None = Field(
        default=None, example=daily_udp_configuration.MAIN_LESSON
    )
    independent_work: list[str] | None = Field(
        default=None, example=daily_udp_configuration.INDEPENDENT_WORK
    )
    closing_home_assignment: list[str] | None = Field(
        default=None,
        example=daily_udp_configuration.CLOSING_HOME_ASSIGNMENT,
    )
    material_needed: list[str] | None = Field(
        default=None, example=daily_udp_configuration.MATERIAL_NEEDED
    )
    success_criteria_teacher: list[str] | None = Field(
        default=None,
        example=daily_udp_configuration.SUCCESS_CRITERIA_TEACHER,
    )
    success_criteria_student: list[str] | None = Field(
        default=None,
        example=daily_udp_configuration.SUCCESS_CRITERIA_STUDENT,
    )
    notes: str | None = Field(
        default=None,
        min_length=1,
        max_length=10_000,
        example=daily_udp_configuration.NOTES,
    )
    status: str | None = Field(
        default=None,
        min_length=1,
        max_length=2_0,
        example=daily_udp_configuration.STATUS,
    )

    # CoreConfiguration Configuration
    model_config = SettingsConfigDict(
        str_strip_whitespace=True, from_attributes=True
    )
